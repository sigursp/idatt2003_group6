
# Chaos Game project IDATT2003 - 2024
STUDENT NAME = "Sigurd Spook"  
STUDENT NAME = André Merkesdal

STUDENT ID = "10047"
STUDENT ID = "10007"


## Project description
The project is taken as part of the IDATT2003 course at NTNU Trondheim. The goal of the project is document general knowledge about inheritence, polymorphism, JavaFX and different design patterns.

The program lets the user calculate differnet affine and julia transformation using a GUI made in JavaFX.

The user can:
* Generate an empty Julia or Affine transformation
* Save parameters to a file
* Read a new transformation from a file.
* Change and load new parameters to see how the transformation changes.
* Scale the application window.
* Quit the application.


## Project structure
source files are stored in src/main/java/edu/ntnu/idatt2003/group6
Here the program is split into directories following the mvc pattern:
* Controller
* Models
* Utils
* View
* App

The main program is the App

## Link to repository
https://gitlab.stud.idi.ntnu.no/sigursp/idatt2003_group6

## How to run the project
The program can be run two ways. As a command line client or as a GUI with javaFX.

To run as a cli. Open the view folder and launch the class CommandLineClient.

To run the main application. Type the command
``` mvn javafx:run ```
Or run it using a maven add on if you have this installed.

## How to run the tests
Open the test direcory in src/test/java
From here you can run test for individual classes. Or all classes by selecting the java folder and clicking 
```
Run 'Test in 'Java"
```