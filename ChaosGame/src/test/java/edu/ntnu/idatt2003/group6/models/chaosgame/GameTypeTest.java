package edu.ntnu.idatt2003.group6.models.chaosgame;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class GameTypeTest {

  @Test
  void testEnumValues() {
    // Test that the enum contains the expected number of values
    assertEquals(6, GameType.values().length);

    // Test that the enum contains the expected values
    assertEquals(GameType.SIERPINSKI, GameType.valueOf("SIERPINSKI"));
    assertEquals(GameType.BARNSLEY, GameType.valueOf("BARNSLEY"));
    assertEquals(GameType.JULIA, GameType.valueOf("JULIA"));
    assertEquals(GameType.AFFINE, GameType.valueOf("AFFINE"));
    assertEquals(GameType.JULIA_NEW, GameType.valueOf("JULIA_NEW"));
    assertEquals(GameType.AFFINE_NEW, GameType.valueOf("AFFINE_NEW"));
  }

}