package edu.ntnu.idatt2003.group6.models.files;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FileStateTest {

  @Test
  void testEnumValues() {
    // Test that the enum contains the expected number of values
    assertEquals(3, FileState.values().length);

    // Test that the enum contains the expected values
    assertEquals(FileState.FILES_UPDATING, FileState.valueOf("FILES_UPDATING"));
    assertEquals(FileState.FILES_UNCHANGED, FileState.valueOf("FILES_UNCHANGED"));
    assertEquals(FileState.FILES_CHANGED, FileState.valueOf("FILES_CHANGED"));
  }
}