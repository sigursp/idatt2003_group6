package edu.ntnu.idatt2003.group6.models.vector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class Vector2DTest {

  Vector2D vector;

  @BeforeEach
  public void setUp() {
    vector = new Vector2D(1, 2);
  }

  @Nested
  @DisplayName("Positive tests for Vector2D")
  class methodsDoesNotThrowException {
    @Test
    @DisplayName("Vector2D constructor creates an instance of Vector2D " +
        "without throwing an exception")
    void vector2DDoesntThrowOnNewVector2D() {
      try {
        Vector2D testVector = new Vector2D(1, 2);
        assertNotNull(testVector);
      } catch (IllegalArgumentException e) {
        fail("The test vector2DDoesntThrowOnNewVector2D failed with the exception " +
            "message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Getter methods return the expected value without throwing an exception")
    void evaluateVector2DGetter() {
      try {
        assertEquals(1, vector.getX0());
        assertEquals(2, vector.getX1());
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Add method returns the expected value without throwing an exception")
    void evaluateVector2DAdd() {
      Vector2D vector2 = new Vector2D(3, 4);
      Vector2D result = vector.add(vector2);
      assertEquals(4, result.getX0());
      assertEquals(6, result.getX1());
    }

    @Test
    @DisplayName("Subtract method returns the expected value without throwing an exception")
    void evaluateVector2DSubtract() {
      Vector2D vector2 = new Vector2D(3, 4);
      Vector2D result = vector.subtract(vector2);
      assertEquals(-2, result.getX0());
      assertEquals(-2, result.getX1());
    }

    @Test
    @DisplayName("MultiplyScalar method returns the expected value without throwing an exception")
    void evaluateVector2DMultiplyScalar() {
      Vector2D result = vector.multiplyScalar(2);
      assertEquals(2, result.getX0());
      assertEquals(4, result.getX1());
    }
  }

  @Nested
  @DisplayName("Negative tests for Vector2D")
  class methodsThrowsException {
    @Test
    @DisplayName("Add method throws an exception when given invalid input")
    void Vector2DAddThrowsOnInvalidInput() {
      try {
        vector.add(null);
        fail("The test Vector2DAddThrowsOnInvalidInput failed");
      } catch (IllegalArgumentException e) {
        assertEquals("The vector can not be null",
            e.getMessage());
      }
    }

    @Test
    @DisplayName("Subtract method throws an exception when given invalid input")
    void Vector2DSubtractThrowsOnInvalidInput() {
      try {
        vector.subtract(null);
        fail("The test Vector2DSubtractThrowsOnInvalidInput failed");
      } catch (IllegalArgumentException e) {
        assertEquals("The vector can not be null",
            e.getMessage());
      }
    }
  }
}