package edu.ntnu.idatt2003.group6.models.chaosgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.transformation.AffineTransform2D;
import edu.ntnu.idatt2003.group6.models.transformation.Transform2D;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import edu.ntnu.idatt2003.group6.utils.exceptions.IllegalChaosGameException;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ChaosGameDescriptionTest {

  ChaosGameDescription chaosGameDescription;
  List<Transform2D> transformations;

  @BeforeEach
  public void setUp() {
    transformations = Arrays.asList(new AffineTransform2D(
        new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)
    ), new AffineTransform2D(
        new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0)
    ), new AffineTransform2D(
        new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5)
    ));
    chaosGameDescription =
        new ChaosGameDescription(transformations, new Vector2D(0, 0), new Vector2D(1, 1));
  }

  @Nested
  @DisplayName("Positive tests for ChaosGameDescription")
  class methodsDoesNotThrowException {
    @Test
    @DisplayName("ChaosGameDescription constructor creates an instance of ChaosGameDescription " +
        "without throwing an exception")
    void chaosGameDescriptionDoesntThrowOnNewChaosGameDescription() {
      try {
        ChaosGameDescription testChaosGameDescription =
            new ChaosGameDescription(transformations, new Vector2D(0, 0), new Vector2D(1, 1));
        assertNotNull(testChaosGameDescription);
      } catch (IllegalChaosGameException e) {
        fail(
            "The test chaosGameDescriptionDoesntThrowOnNewChaosGameDescription failed with the exception " +
                "message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getTransformations method returns the expected value without throwing an exception")
    void evaluateChaosGameDescriptionGetTransformations() {
      List<Transform2D> result = chaosGameDescription.getTransformations();
      assertEquals(transformations, result);
    }

    @Test
    @DisplayName("getMinCoords method returns the expected value without throwing an exception")
    void evaluateChaosGameDescriptionGetMinCoords() {
      Vector2D result = chaosGameDescription.getMinCoords();
      assertEquals(0.0, result.getX0());
      assertEquals(0.0, result.getX1());
    }

    @Test
    @DisplayName("getMaxCoords method returns the expected value without throwing an exception")
    void evaluateChaosGameDescriptionGetMaxCoords() {
      Vector2D result = chaosGameDescription.getMaxCoords();
      assertEquals(1.0, result.getX0());
      assertEquals(1.0, result.getX1());
    }

    @Test
    @DisplayName("setTransformations method sets the transformations correctly")
    void evaluateChaosGameDescriptionSetTransformations() {
      List<Transform2D> newTransformations = Arrays.asList(new AffineTransform2D(
          new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)
      ), new AffineTransform2D(
          new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0)
      ), new AffineTransform2D(
          new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5)
      ));
      chaosGameDescription.setTransformations(newTransformations);
      assertEquals(newTransformations, chaosGameDescription.getTransformations());
    }

    @Test
    @DisplayName("setMinCoords method sets the minimum coordinates correctly")
    void evaluateChaosGameDescriptionSetMinCoords() {
      Vector2D newMinCoords = new Vector2D(0.5, 0.5);
      chaosGameDescription.setMinCoords(newMinCoords);
      assertEquals(newMinCoords, chaosGameDescription.getMinCoords());
    }

    @Test
    @DisplayName("setMaxCoords method sets the maximum coordinates correctly")
    void evaluateChaosGameDescriptionSetMaxCoords() {
      Vector2D newMaxCoords = new Vector2D(1.5, 1.5);
      chaosGameDescription.setMaxCoords(newMaxCoords);
      assertEquals(newMaxCoords, chaosGameDescription.getMaxCoords());
    }

    @Test
    @DisplayName("getGameType method returns the expected value")
    void evaluateChaosGameDescriptionGetGameType() {
      GameType result = chaosGameDescription.getGameType();

      assertEquals(GameType.AFFINE, result);
    }
  }

  @Nested
  @DisplayName("Negative tests for ChaosGameDescription")
  class MethodsThrowsExceptions {
    @Test
    @DisplayName("ChaosGameDescription constructor throws an exception when transformations is null")
    void chaosGameDescriptionThrowsOnNullTransformations() {
      assertThrows(IllegalChaosGameException.class, () ->
          new ChaosGameDescription(null, new Vector2D(0, 0), new Vector2D(1, 1)));
    }

    @Test
    @DisplayName("ChaosGameDescription constructor throws an exception when minCoords is null")
    void chaosGameDescriptionThrowsOnNullMinCoords() {
      assertThrows(IllegalChaosGameException.class, () ->
          new ChaosGameDescription(transformations, null, new Vector2D(1, 1)));
    }

    @Test
    @DisplayName("ChaosGameDescription constructor throws an exception when maxCoords is null")
    void chaosGameDescriptionThrowsOnNullMaxCoords() {
      assertThrows(IllegalChaosGameException.class, () ->
          new ChaosGameDescription(transformations, new Vector2D(0, 0), null));
    }
  }
}