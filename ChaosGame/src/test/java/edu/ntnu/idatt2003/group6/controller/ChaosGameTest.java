package edu.ntnu.idatt2003.group6.controller;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGame;
import edu.ntnu.idatt2003.group6.controller.chaosgame.GameState;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosCanvas;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ChaosGameTest {

  private ChaosGame chaosGame;
  private ChaosGameDescription chaosGameDescription;

  @BeforeEach
  void setUp() {
    chaosGameDescription =
        ChaosGameDescriptionFactory.createChaosGameDescription(GameType.SIERPINSKI);
    chaosGame = new ChaosGame(chaosGameDescription, 700, 700);
  }

  @Nested
  @DisplayName("Positive tests for ChaosGame")
  class methodsDoesNotThrowException {

    @Test
    @DisplayName("ChaosGame constructor creates an instance of ChaosGame without throwing an exception")
    void chaosGameDoesntThrowOnNewChaosGame() {
      assertNotNull(chaosGame);
    }

    @Test
    @DisplayName("getState method returns the expected value without throwing an exception")
    void evaluateChaosGameGetState() {
      chaosGame.runSteps(1);
      GameState result = chaosGame.getState();
      assertEquals(GameState.DONE, result);
    }

    @Test
    @DisplayName("runSteps method executes without throwing an exception")
    void runStepsDoesntThrow() {
      assertDoesNotThrow(() -> chaosGame.runSteps(1));
    }

    @Test
    @DisplayName("addObserver method executes without throwing an exception")
    void addObserverDoesntThrow() {
      assertDoesNotThrow(() -> chaosGame.addObserver((o) -> {
      }));
    }

    @Test
    @DisplayName("removeObserver method executes without throwing an exception")
    void removeObserverDoesntThrow() {
      assertDoesNotThrow(() -> chaosGame.removeObserver((o) -> {
      }));
    }

    @Test
    @DisplayName("ChaosGame constructor throws on null description")
    void chaosGameThrowsOnNullDescription() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGame(null, 700, 700));
    }

    @Test
    @DisplayName("ChaosGame constructor throws on invalid width")
    void chaosGameThrowsOnInvalidWidth() {
      assertThrows(IllegalArgumentException.class,
          () -> new ChaosGame(chaosGameDescription, 0, 700));
    }

    @Test
    @DisplayName("ChaosGame constructor throws on invalid height")
    void chaosGameThrowsOnInvalidHeight() {
      assertThrows(IllegalArgumentException.class,
          () -> new ChaosGame(chaosGameDescription, 700, 0));
    }

    @Test
    @DisplayName("getCanvas method returns the canvas")
    void evaluateChaosGameGetCanvas() {
      ChaosCanvas result = chaosGame.getCanvas();
      assertNotNull(result);
    }

    @Test
    @DisplayName("getChaosGameDescription method returns the chaos game description")
    void evaluateChaosGameGetChaosGameDescription() {
      ChaosGameDescription result = chaosGame.getChaosGameDescription();
      assertNotNull(result);
    }

    @Test
    @DisplayName("getGameType method returns the game type")
    void evaluateChaosGameGetGameType() {
      GameType result = chaosGame.getGameType();
      assertEquals(GameType.AFFINE, result);
    }


  }

  @Nested
  @DisplayName("Negative tests for ChaosGame")
  class methodsThrowsExceptions {

    @Test
    @DisplayName("ChaosGame constructor throws an exception when description is null")
    void chaosGameThrowsOnNullDescription() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosGame(null, 700, 700));
    }

    @Test
    @DisplayName("ChaosGame constructor throws an exception when width is less than or equal to 0")
    void chaosGameThrowsOnInvalidWidth() {
      assertThrows(IllegalArgumentException.class,
          () -> new ChaosGame(chaosGameDescription, 0, 700));
    }

    @Test
    @DisplayName("ChaosGame constructor throws an exception when height is less than or equal to 0")
    void chaosGameThrowsOnInvalidHeight() {
      assertThrows(IllegalArgumentException.class,
          () -> new ChaosGame(chaosGameDescription, 700, 0));
    }

    @Test
    @DisplayName("runSteps method throws an exception when steps is less than 1")
    void runStepsThrowsOnInvalidSteps() {
      assertThrows(IllegalArgumentException.class, () -> chaosGame.runSteps(-1));
    }

    @Test
    @DisplayName("addObserver method throws an exception when observer is null")
    void addObserverThrowsOnNullObserver() {
      assertThrows(IllegalArgumentException.class, () -> chaosGame.addObserver(null));
    }

    @Test
    @DisplayName("removeObserver method throws an exception when observer is null")
    void removeObserverThrowsOnNullObserver() {
      assertThrows(IllegalArgumentException.class, () -> chaosGame.removeObserver(null));
    }
  }
}