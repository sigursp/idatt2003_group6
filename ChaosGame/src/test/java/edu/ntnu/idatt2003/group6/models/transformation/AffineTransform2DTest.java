package edu.ntnu.idatt2003.group6.models.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class AffineTransform2DTest {

  Matrix2x2 matrix;
  Vector2D vector;
  AffineTransform2D affineTransform2D;

  @BeforeEach
  public void setUp() {
    matrix = new Matrix2x2(1, 2, 3, 4);
    vector = new Vector2D(1, 2);
    affineTransform2D = new AffineTransform2D(matrix, vector);
  }

  @Nested
  @DisplayName("Positive tests for AffineTransform2D")
  class methodsDoesNotThrowException {

    @Test
    @DisplayName("Record fields are set correctly")
    void testRecordFields() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
      Vector2D vector = new Vector2D(1, 2);
      AffineTransform2D transform = new AffineTransform2D(matrix, vector);

      assertEquals(matrix, transform.matrix());
      assertEquals(vector, transform.vector());
    }

    @Test
    @DisplayName("AffineTransform2D constructor creates an instance of AffineTransform2D " +
        "without throwing an exception")
    void affineTransform2DDoesntThrowOnNewAffineTransform2D() {
      try {
        AffineTransform2D testAffineTransform2D = new AffineTransform2D(matrix, vector);
        assertNotNull(testAffineTransform2D);
      } catch (IllegalArgumentException e) {
        fail(
            "The test affineTransform2DDoesntThrowOnNewAffineTransform2D failed with the exception " +
                "message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("matrix method returns the expected value without throwing an exception")
    void matrixDoesntThrow() {
      assertEquals(matrix, affineTransform2D.matrix());
    }

    @Test
    @DisplayName("vector method returns the expected value without throwing an exception")
    void vectorDoesntThrow() {
      assertEquals(vector, affineTransform2D.vector());
    }

    @Test
    @DisplayName("Transform method returns the expected value without throwing an exception")
    void evaluateAffineTransform2DTransform() {
      Vector2D result = new AffineTransform2D(matrix, vector).transform(vector);
      assertEquals(6, result.getX0());
      assertEquals(13, result.getX1());
    }

    @Test
    @DisplayName("gameType method returns the expected value without throwing an exception")
    void gameTypeDoesntThrow() {
      assertEquals(GameType.AFFINE, affineTransform2D.gameType());
    }

    @Test
    void testToString() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
      Vector2D vector = new Vector2D(1, 2);
      AffineTransform2D transform = new AffineTransform2D(matrix, vector);

      String expected = "AffineTransform2D[matrix=" + matrix + ", vector=" + vector + "]";
      assertEquals(expected, transform.toString());
    }

  }

  @Nested
  @DisplayName("Negative tests for AffineTransform2D")
  class methodsThrowsExceptions {

    @Test
    @DisplayName("AffineTransform2D constructor throws an exception when matrix is null")
    void affineTransform2DThrowsOnNullMatrix() {
      assertThrows(IllegalArgumentException.class, () -> new AffineTransform2D(null, vector));
    }

    @Test
    @DisplayName("AffineTransform2D constructor throws an exception when vector is null")
    void affineTransform2DThrowsOnNullVector() {
      assertThrows(IllegalArgumentException.class, () -> new AffineTransform2D(matrix, null));
    }

    @Test
    @DisplayName("transform method throws an exception when vector is null")
    void transformThrowsOnNullVector() {
      assertThrows(IllegalArgumentException.class, () -> affineTransform2D.transform(null));
    }
  }

}