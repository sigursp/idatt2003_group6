package edu.ntnu.idatt2003.group6.models.vector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2003.group6.models.vector.Complex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ComplexTest {

  Complex complex;

  @BeforeEach
  public void setUp() {
    complex = new Complex(1, 2);
  }

  @Nested
  @DisplayName("Positive tests for Complex")
  class methodsDoesNotThrowException {
    @Test
    public void testComplexConstructor() {
      try {
        Complex testComplex = new Complex(1, 2);
        assertNotNull(testComplex);
      } catch (IllegalArgumentException e) {
        fail("The test failed with the exception message " + e.getMessage());
      }
    }

    @Test
    public void testComplexSqrt() {
      Complex result = complex.sqrt();
      assertEquals(1.272019649514069, result.getX0());
      assertEquals(0.7861513777574233, result.getX1());
    }
  }
}