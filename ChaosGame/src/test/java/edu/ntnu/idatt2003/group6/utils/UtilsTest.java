package edu.ntnu.idatt2003.group6.utils;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class UtilsTest {

  @Nested
  @DisplayName("Positive tests for Utils")
  class utilsDoesNotThrowException {

    @Test
    @DisplayName("verifyInt method does not throw an exception when given a positive integer")
    void verifyIntDoesNotThrowOnPositiveInt() {
      assertDoesNotThrow(() -> Utils.verifyInt(1, "test"));
    }

    @Test
    @DisplayName("verifyInt method does not throw an exception when given a negative integer")
    void verifyIntDoesNotThrowOnNegativeInt() {
      assertDoesNotThrow(() -> Utils.verifyInt(-1, "test"));
    }

    @Test
    @DisplayName("verifyInt method throws an exception when given 0")
    void verifyIntThrowsOnZero() {
      assertThrows(IllegalArgumentException.class, () -> Utils.verifyInt(0, "test"));
    }

    @Test
    @DisplayName("inputStringToInt method returns the expected value without throwing an exception")
    void inputStringToIntReturnsExpectedValue() {
      assertEquals(10, Utils.inputStringToInt("10"));
    }

    @Test
    @DisplayName("inputStringToInt method throws an exception when given a non-integer string")
    void inputStringToIntThrowsOnNonIntegerString() {
      assertThrows(IllegalArgumentException.class, () -> Utils.inputStringToInt("test"));
    }

    @Test
    @DisplayName("inputStringToDouble method returns the expected value without throwing an exception")
    void inputStringToDoubleReturnsExpectedValue() {
      assertEquals(10.5, Utils.inputStringToDouble("10.5"));
    }

    @Test
    @DisplayName("inputStringToDouble method throws an exception when given a non-double string")
    void inputStringToDoubleThrowsOnNonDoubleString() {
      assertThrows(IllegalArgumentException.class, () -> Utils.inputStringToDouble("test"));
    }
  }

  @Nested
  @DisplayName("Negative tests for Utils")
  class utilsThrowsException {

    @Test
    @DisplayName("verifyInt method throws an exception when given a null string")
    void verifyIntThrowsOnNullString() {
      assertThrows(IllegalArgumentException.class, () -> Utils.verifyInt(0, "test"));
    }

    @Test
    @DisplayName("inputStringToInt method throws an exception when given a null string")
    void inputStringToIntThrowsOnNullString() {
      assertThrows(IllegalArgumentException.class, () -> Utils.inputStringToInt(null));
    }

    @Test
    @DisplayName("inputStringToDouble method throws an exception when given a null string")
    void inputStringToDoubleThrowsOnNullString() {
      assertThrows(IllegalArgumentException.class, () -> Utils.inputStringToDouble(null));
    }
  }
}