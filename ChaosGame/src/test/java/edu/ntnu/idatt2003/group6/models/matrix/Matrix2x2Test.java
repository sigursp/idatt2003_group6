package edu.ntnu.idatt2003.group6.models.matrix;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class Matrix2x2Test {

  Matrix2x2 matrix;

  @BeforeEach
  public void setUp() {
    matrix = new Matrix2x2(1, 2, 3, 4);
  }

  @Nested
  @DisplayName("Positive tests for Matrix2x2")
  class methodsDoesNotThrowException {

    @Test
    void testRecordFields() {
      Matrix2x2 matrix = new Matrix2x2(1, 2, 3, 4);
      assertEquals(1, matrix.a00());
      assertEquals(2, matrix.a01());
      assertEquals(3, matrix.a10());
      assertEquals(4, matrix.a11());
    }

    @Test
    @DisplayName("Matrix2x2 constructor creates an instance of Matrix2x2 " +
        "without throwing an exception")
    void matrix2x2DoesntThrowOnNewMatrix2x2() {
      try {
        Matrix2x2 testMatrix = new Matrix2x2(1, 2, 3, 4);
        assertNotNull(testMatrix);
      } catch (IllegalArgumentException e) {
        fail("The test matrix2x2DoesntThrowOnNewMatrix2x2 failed with the exception " +
            "message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Multiply method returns the expected value without throwing an exception")
    void evaluateMatrix2x2Multiply() {
      Vector2D vector = new Vector2D(1, 2);
      Vector2D result = matrix.multiply(vector);
      assertEquals(5, result.getX0());
      assertEquals(11, result.getX1());
    }
  }

  @Nested
  @DisplayName("Negative tests for Matrix2x2")
  class methodsThrowsExceptions {
    @Test
    @DisplayName("Matrix2x2 multiply throws an exception when the vector is invalid")
    void matrix2x2ThrowsOnInvalidVector() {
      try {
        matrix.multiply(null);
        fail("The test matrix2x2ThrowsOnInvalidVector failed");
      } catch (IllegalArgumentException e) {
        assertEquals("The given vector cannot be null.", e.getMessage());
      }
    }

  }
}