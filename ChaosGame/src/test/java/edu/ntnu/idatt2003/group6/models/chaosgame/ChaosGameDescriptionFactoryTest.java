package edu.ntnu.idatt2003.group6.models.chaosgame;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ChaosGameDescriptionFactoryTest {


  @Nested
  @DisplayName("Positive tests for ChaosGameDescriptionFactory")
  class methodsDoesNotThrowException {
    @Test
    @DisplayName("createChaosGameDescription method creates an instance of Sierpinski " +
        "without throwing an exception")
    void createChaosGameDescriptionSierpinskiDoesntThrowOnNewChaosGameDescription() {
      try {
        ChaosGameDescription testChaosGameDescription =
            ChaosGameDescriptionFactory.createChaosGameDescription(GameType.SIERPINSKI);
        assertNotNull(testChaosGameDescription);
      } catch (Exception e) {
        fail(
            "The test createChaosGameDescriptionDoesntThrowOnNewChaosGameDescription failed" +
                "with the exception message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("createChaosGameDescription method creates an instance of Barnsley " +
        "without throwing an exception")
    void createChaosGameDescriptionBarnsleyDoesntThrowOnNewChaosGameDescription() {
      try {
        ChaosGameDescription testChaosGameDescription =
            ChaosGameDescriptionFactory.createChaosGameDescription(GameType.BARNSLEY);
        assertNotNull(testChaosGameDescription);
      } catch (Exception e) {
        fail(
            "The test createChaosGameDescriptionBarnsleyDoesntThrowOnNewChaosGameDescription " +
                "failed with the exception message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("createChaosGameDescription method creates an instance of Julia " +
        "without throwing an exception")
    void createChaosGameDescriptionJuliaDoesntThrowOnNewChaosGameDescription() {
      try {
        ChaosGameDescription testChaosGameDescription =
            ChaosGameDescriptionFactory.createChaosGameDescription(GameType.JULIA);
        assertNotNull(testChaosGameDescription);
      } catch (Exception e) {
        fail(
            "The test createChaosGameDescriptionJuliaDoesntThrowOnNewChaosGameDescription " +
                "failed with the exception message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("createChaosGameDescription method creates an instance of Julia_New " +
        "without throwing an exception")
    void createChaosGameDescriptionJuliaNewDoesntThrowOnNewChaosGameDescription() {
      try {
        ChaosGameDescription testChaosGameDescription =
            ChaosGameDescriptionFactory.createChaosGameDescription(GameType.JULIA_NEW);
        assertNotNull(testChaosGameDescription);
      } catch (Exception e) {
        fail(
            "The test createChaosGameDescriptionJuliaNewDoesntThrowOnNewChaosGameDescription " +
                "failed with the exception message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("createChaosGameDescription method creates an instance of Affine_New " +
        "without throwing an exception")
    void createChaosGameDescriptionAffineNewDoesntThrowOnNewChaosGameDescription() {
      try {
        ChaosGameDescription testChaosGameDescription =
            ChaosGameDescriptionFactory.createChaosGameDescription(GameType.AFFINE_NEW);
        assertNotNull(testChaosGameDescription);
      } catch (Exception e) {
        fail(
            "The test createChaosGameDescriptionAffineNewDoesntThrowOnNewChaosGameDescription " +
                "failed with the exception message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("createChaosGameDescription method throws an exception when gameType is null")
    void createChaosGameDescriptionThrowsOnNullGameType() {
      assertThrows(IllegalArgumentException.class, () ->
          ChaosGameDescriptionFactory.createChaosGameDescription(null));
    }

    @Test
    @DisplayName("createChaosGameDescription method throws an exception when gameType is invalid")
    void createChaosGameDescriptionThrowsOnInvalidGameType() {
      assertThrows(IllegalArgumentException.class, () ->
          ChaosGameDescriptionFactory.createChaosGameDescription(GameType.valueOf("INVALID")));
    }

  }
}