package edu.ntnu.idatt2003.group6.models.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.vector.Complex;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class JuliaTransformTest {

  Complex complex;
  int complexInt;

  @BeforeEach
  public void setUp() {
    complex = new Complex(1, 2);
    complexInt = 1;
  }

  @Nested
  @DisplayName("Positive tests for JuliaTransform")
  public class methodsDoesNotThrowException {
    @Test
    @DisplayName("JuliaTransform constructor creates an instance of JuliaTransform " +
        "without throwing an exception")
    void juliaTransformDoesntThrowOnNewJuliaTransform() {
      try {
        JuliaTransform testJuliaTransform = new JuliaTransform(complex, complexInt);
        assertNotNull(testJuliaTransform);
      } catch (IllegalArgumentException e) {
        fail("The test juliaTransformDoesntThrowOnNewJuliaTransform failed with the exception " +
            "message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Transform method returns the expected value without throwing an exception")
    void evaluateJuliaTransformTransform() {
      JuliaTransform juliaTransform = new JuliaTransform(complex, complexInt);
      Vector2D result = new Vector2D(1, 1);
      juliaTransform.transform(result);
      assertEquals(1, result.getX0());
      assertEquals(1, result.getX1());
    }

    @Test
    @DisplayName("Transform method returns the expected value without throwing an exception")
    void evaluateJuliaTransformTransform2() {
      JuliaTransform juliaTransform = new JuliaTransform(complex, -1);
      Vector2D result = new Vector2D(1, 1);
      juliaTransform.transform(result);
      assertEquals(1, result.getX0());
      assertEquals(1, result.getX1());
    }

    @Test
    @DisplayName("GetPoint method returns the expected value without throwing an exception")
    void evaluateJuliaTransformGetPoint() {
      JuliaTransform juliaTransform = new JuliaTransform(complex, complexInt);
      assertEquals(complex, juliaTransform.getPoint());
    }

    @Test
    @DisplayName("GameType method returns the expected value without throwing an exception")
    void evaluateJuliaTransformGameType() {
      JuliaTransform juliaTransform = new JuliaTransform(complex, complexInt);
      assertEquals(GameType.JULIA, juliaTransform.gameType());
    }

  }

  @Nested
  @DisplayName("Negative tests for JuliaTransform")
  public class methodsThrowException {
    @Test
    @DisplayName("JuliaTransform constructor throws an exception when the sign is not +-1")
    void juliaTransformThrowsOnInvalidSign() {
      try {
        new JuliaTransform(complex, 0);
        fail("The test juliaTransformThrowsOnInvalidSign failed");
      } catch (IllegalArgumentException e) {
        assertEquals("The sign must be either 1 or -1.", e.getMessage());
      }
    }
  }
}