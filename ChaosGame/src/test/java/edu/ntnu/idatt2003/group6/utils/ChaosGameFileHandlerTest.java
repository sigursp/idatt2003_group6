package edu.ntnu.idatt2003.group6.utils;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

class ChaosGameFileHandlerTest {
  private ChaosGameDescription chaosGameDescriptionAffine;
  private ChaosGameDescription chaosGameDescriptionJulia;
  private String fileName;

  @BeforeEach
  void setUp() {
    chaosGameDescriptionAffine =
        ChaosGameDescriptionFactory.createChaosGameDescription(GameType.SIERPINSKI);
    chaosGameDescriptionJulia =
        ChaosGameDescriptionFactory.createChaosGameDescription(GameType.JULIA);
    fileName = "testFile";
  }

  @Nested
  @DisplayName("Positive tests for ChaosGameFileHandler")
  class methodsDoesNotThrowException {

    @Test
    @DisplayName("writeChaosGameDescriptionToFile method writes to file without throwing an exception")
    void writeChaosGameDescriptionAffineToFileDoesntThrow() {
      assertDoesNotThrow(() -> ChaosGameFileHandler.writeChaosGameDescriptionToFile(
          fileName, chaosGameDescriptionAffine));
    }

    @Test
    @DisplayName("readChaosGameDescriptionFromFile method reads from file without throwing an exception")
    void readChaosGameDescriptionAffineFromFileDoesntThrow() {
      assertDoesNotThrow(() -> ChaosGameFileHandler.readChaosGameDescriptionFromFile(fileName));
    }

    @Test
    @DisplayName("writeChaosGameDescriptionToFile method writes a ChaosGameDescriptionJulia to file without throwing an exception")
    void writeChaosGameDescriptionJuliaToFileDoesntThrow() {
      assertDoesNotThrow(() -> ChaosGameFileHandler.writeChaosGameDescriptionToFile(fileName,
          chaosGameDescriptionJulia));
    }

    @Test
    @DisplayName("readChaosGameDescriptionFromFile method reads a ChaosGameDescriptionJulia from file without throwing an exception")
    void readChaosGameDescriptionJuliaFromFileDoesntThrow() {
      ChaosGameFileHandler.writeChaosGameDescriptionToFile(fileName, chaosGameDescriptionJulia);
      assertDoesNotThrow(() -> ChaosGameFileHandler.readChaosGameDescriptionFromFile(fileName));
    }

    @Test
    @DisplayName("readChaosGameDescriptionFromFile method reads a ChaosGameDescriptionJulia from file correctly")
    void readChaosGameDescriptionFromFileCorrectly() {
      ChaosGameFileHandler.writeChaosGameDescriptionToFile(fileName, chaosGameDescriptionJulia);
      ChaosGameDescription readChaosGameDescriptionJulia =
          ChaosGameFileHandler.readChaosGameDescriptionFromFile(fileName);
      assertEquals(chaosGameDescriptionJulia.getTransformations().getFirst().gameType(),
          readChaosGameDescriptionJulia.getTransformations().getFirst().gameType());
      assertEquals(chaosGameDescriptionJulia.getMinCoords().getX0(),
          readChaosGameDescriptionJulia.getMinCoords().getX0());
      assertEquals(chaosGameDescriptionJulia.getMinCoords().getX1(),
          readChaosGameDescriptionJulia.getMinCoords().getX1());
      assertEquals(chaosGameDescriptionJulia.getMaxCoords().getX0(),
          readChaosGameDescriptionJulia.getMaxCoords().getX0());
      assertEquals(chaosGameDescriptionJulia.getMaxCoords().getX1(),
          readChaosGameDescriptionJulia.getMaxCoords().getX1());
      assertEquals(GameType.JULIA, readChaosGameDescriptionJulia.getGameType());
    }

    @Test
    @DisplayName("showFiles method shows files without throwing an exception")
    void showFilesDoesntThrow() {
      assertDoesNotThrow(() -> ChaosGameFileHandler.showFiles("."));
    }

    @Test
    @DisplayName("numberOfFiles method counts files without throwing an exception")
    void numberOfFilesDoesntThrow() {
      assertDoesNotThrow(() -> ChaosGameFileHandler.numberOfFiles("."));
    }

    @Test
    @DisplayName("deleteFile method deletes file without throwing an exception")
    void deleteFileDoesntThrow() {
      assertDoesNotThrow(() -> ChaosGameFileHandler.deleteFile(fileName));
    }
  }

  @Nested
  @DisplayName("Negative tests for ChaosGameFileHandler")
  class methodsThrowsExceptions {

    @Test
    @DisplayName("writeChaosGameDescriptionToFile method throws an exception when fileName is null")
    void writeChaosGameDescriptionToFileThrowsOnNullFileName() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.writeChaosGameDescriptionToFile(
              null, chaosGameDescriptionAffine));
    }

    @Test
    @DisplayName("writeChaosGameDescriptionToFile method throws an exception when chaosGameDescription is null")
    void writeChaosGameDescriptionToFileThrowsOnNullChaosGameDescription() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.writeChaosGameDescriptionToFile(
              fileName, null));
    }

    @Test
    @DisplayName("readChaosGameDescriptionFromFile method throws an exception when fileName is null")
    void readChaosGameDescriptionFromFileThrowsOnNullFileName() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.readChaosGameDescriptionFromFile(null));
    }

    @Test
    @DisplayName("showFiles method throws an exception when directoryPath is null")
    void showFilesThrowsOnNullDirectoryPath() {
      assertThrows(RuntimeException.class, () -> ChaosGameFileHandler.showFiles(null));
    }

    @Test
    @DisplayName("numberOfFiles method throws an exception when directoryPath is null")
    void numberOfFilesThrowsOnNullDirectoryPath() {
      assertThrows(RuntimeException.class, () -> ChaosGameFileHandler.numberOfFiles(null));
    }

    @Test
    @DisplayName("deleteFile method throws an exception when path is null")
    void deleteFileThrowsOnNullPath() {
      assertThrows(RuntimeException.class, () -> ChaosGameFileHandler.deleteFile(null));
    }

    @TempDir
    Path tempDir;

    @Test
    void testReadFromFileIOException() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.readFromFile(null));
    }

    @Test
    void testWriteToFileExtraIOException() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.writeToFileExtra(null, "content"));
    }

    @Test
    void testWriteToFileIOException() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.writeToFile(null, "content"));
    }

    @Test
    void testShowFilesIOException() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.showFiles("nonexistentdirectory"));
    }

    @Test
    void testNumberOfFilesIOException() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.numberOfFiles("nonexistentdirectory"));
    }

    @Test
    void testDeleteFileIOException() {
      assertThrows(RuntimeException.class,
          () -> ChaosGameFileHandler.deleteFile(null));
    }

    @Test
    void testDeleteFileSuccess() throws IOException {
      Path file = Files.createFile(tempDir.resolve("testfile.txt"));
      assertDoesNotThrow(() -> ChaosGameFileHandler.deleteFile(file.toString()));
      assertFalse(Files.exists(file));
    }
  }
}