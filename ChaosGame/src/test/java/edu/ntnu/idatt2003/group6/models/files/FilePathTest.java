package edu.ntnu.idatt2003.group6.models.files;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FilePathTest {

  @Test
  void testEnumValues() {
    // Test that the enum contains the expected number of values
    assertEquals(3, FilePath.values().length);
    // Test that the enum contains the expected values
    assertEquals(FilePath.DESCRIPTIONS, FilePath.valueOf("DESCRIPTIONS"));
    assertEquals(FilePath.IMAGES, FilePath.valueOf("IMAGES"));
    assertEquals(FilePath.MUSIC, FilePath.valueOf("MUSIC"));
    assertEquals("src/main/resources/descriptions/", FilePath.DESCRIPTIONS.getPath());
  }
}