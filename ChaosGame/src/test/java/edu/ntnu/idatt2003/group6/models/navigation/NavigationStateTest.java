package edu.ntnu.idatt2003.group6.models.navigation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NavigationStateTest {

  @Test
  void testEnumValues() {
    // Test that the enum contains the expected number of values
    assertEquals(9, NavigationState.values().length);

    // Test that the enum contains the expected values
    assertEquals(NavigationState.SELECT_FILE, NavigationState.valueOf("SELECT_FILE"));
    assertEquals(NavigationState.EDIT_FILE, NavigationState.valueOf("EDIT_FILE"));
    assertEquals(NavigationState.SELECT_GAME, NavigationState.valueOf("SELECT_GAME"));
    assertEquals(NavigationState.SELECT_GAME_FILES, NavigationState.valueOf("SELECT_GAME_FILES"));
    assertEquals(NavigationState.PLAY_GAME, NavigationState.valueOf("PLAY_GAME"));
    assertEquals(NavigationState.PLAY_JULIA, NavigationState.valueOf("PLAY_JULIA"));
    assertEquals(NavigationState.PLAY_AFFINE, NavigationState.valueOf("PLAY_AFFINE"));
    assertEquals(NavigationState.SETTINGS, NavigationState.valueOf("SETTINGS"));
    assertEquals(NavigationState.MENU, NavigationState.valueOf("MENU"));
  }
}