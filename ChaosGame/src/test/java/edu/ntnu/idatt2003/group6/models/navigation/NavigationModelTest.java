package edu.ntnu.idatt2003.group6.models.navigation;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class NavigationModelTest {

  private NavigationModel navigationModel;

  @BeforeEach
  void setUp() {
    navigationModel = new NavigationModel();
  }

  @Nested
  @DisplayName("Positive tests for NavigationModel")
  class methodsDoesNotThrowException {

    @Test
    @DisplayName("getState method returns the expected value without throwing an exception")
    void getStateDoesntThrow() {
      navigationModel.setState(NavigationState.MENU);
      assertEquals(NavigationState.MENU, navigationModel.getState());
    }

    @Test
    @DisplayName("setState method sets the state correctly without throwing an exception")
    void setStateDoesntThrow() {
      navigationModel.setState(NavigationState.SETTINGS);
      assertEquals(NavigationState.SETTINGS, navigationModel.getState());
    }

    @Test
    @DisplayName("addObserver method adds an observer correctly without throwing an exception")
    void addObserverDoesntThrow() {
      NavigationObserver observer = state -> {};
      assertDoesNotThrow(() -> navigationModel.addObserver(observer));
    }

    @Test
    @DisplayName("removeObserver method removes an observer correctly without throwing an exception")
    void removeObserverDoesntThrow() {
      NavigationObserver observer = state -> {};
      navigationModel.addObserver(observer);
      assertDoesNotThrow(() -> navigationModel.removeObserver(observer));
    }
  }

  @Nested
  @DisplayName("Negative tests for NavigationModel")
  class methodsThrowsExceptions {

    @Test
    @DisplayName("setState method throws an exception when state is null")
    void setStateThrowsOnNullState() {
      assertThrows(IllegalArgumentException.class, () -> navigationModel.setState(null));
    }

    @Test
    @DisplayName("addObserver method throws an exception when observer is null")
    void addObserverThrowsOnNullObserver() {
      assertThrows(IllegalArgumentException.class, () -> navigationModel.addObserver(null));
    }

    @Test
    @DisplayName("removeObserver method throws an exception when observer is null")
    void removeObserverThrowsOnNullObserver() {
      assertThrows(IllegalArgumentException.class, () -> navigationModel.removeObserver(null));
    }
  }
}