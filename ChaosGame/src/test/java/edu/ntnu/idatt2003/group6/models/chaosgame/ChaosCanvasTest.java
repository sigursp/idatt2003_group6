package edu.ntnu.idatt2003.group6.models.chaosgame;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ChaosCanvasTest {

  ChaosCanvas chaosCanvas;

  @BeforeEach
  public void setUp() {
    chaosCanvas = new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
  }

  @Nested
  @DisplayName("Positive tests for ChaosCanvas")
  class methodsDoesNotThrowException {
    @Test
    @DisplayName("ChaosCanvas constructor creates an instance of ChaosCanvas " +
        "without throwing an exception")
    void chaosCanvasDoesntThrowOnNewChaosCanvas() {
      try {
        ChaosCanvas testChaosCanvas =
            new ChaosCanvas(100, 100, new Vector2D(0, 0), new Vector2D(1, 1));
        assertNotNull(testChaosCanvas);
      } catch (IllegalArgumentException e) {
        fail("The test chaosCanvasDoesntThrowOnNewChaosCanvas failed with the exception " +
            "message " + e.getMessage());
      }
    }

    @Test
    @DisplayName("getPixel method returns the expected value without throwing an exception")
    void evaluateChaosCanvasGetPixel() {
      Vector2D point = new Vector2D(0.5, 0.5);
      int result = chaosCanvas.getPixel(point);
      assertEquals(0, result);
    }

    @Test
    @DisplayName("putPixel method returns the expected value without throwing an exception")
    void evaluateChaosCanvasPutPixel() {
      Vector2D point = new Vector2D(0.5, 0.5);
      chaosCanvas.putPixel(point);
      int result = chaosCanvas.getPixel(point);
      assertEquals(1, result);
    }

    @Test
    @DisplayName("getCanvasArray method returns the expected value without throwing an exception")
    void evaluateChaosCanvasGetCanvasArray() {
      int[][] result = chaosCanvas.getCanvasArray();
      assertEquals(100, result.length);
      assertEquals(100, result[0].length);
    }

    @Test
    @DisplayName("clear method returns the expected value without throwing an exception")
    void evaluateChaosCanvasClear() {
      chaosCanvas.putPixel(new Vector2D(0.5, 0.5));
      chaosCanvas.clear();
      int[][] result = chaosCanvas.getCanvasArray();
      assertEquals(0, result[50][50]);
    }

    @Test
    @DisplayName("getMatrixForIndices method returns the expected value without throwing an exception")
    void evaluateChaosCanvasGetMatrixForIndices() {
      try {
        var method = ChaosCanvas.class.getDeclaredMethod("getMatrixForIndices");
        method.setAccessible(true);
        var result = (Matrix2x2) method.invoke(chaosCanvas);
        assertNotNull(result);
      } catch (Exception e) {
        fail("The test evaluateChaosCanvasGetMatrixForIndices failed with the exception " +
            "message " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative tests for ChaosCanvas")
  class methodsThrowsExceptions {
    @Test
    @DisplayName("ChaosCanvas constructor throws an exception when width is less than or equal to 0")
    void chaosCanvasThrowsOnInvalidWidth() {
      assertThrows(IllegalArgumentException.class, () ->
          new ChaosCanvas(0, 100, new Vector2D(0, 0), new Vector2D(1, 1)));
    }

    @Test
    @DisplayName("ChaosCanvas constructor throws an exception when height is less than or equal to 0")
    void chaosCanvasThrowsOnInvalidHeight() {
      assertThrows(IllegalArgumentException.class, () ->
          new ChaosCanvas(100, 0, new Vector2D(0, 0), new Vector2D(1, 1)));
    }

    @Test
    @DisplayName("getPixel method throws an exception when point is out of bounds")
    void evaluateChaosCanvasGetPixelThrows() {
      Vector2D point = new Vector2D(2, 2);
      assertThrows(IllegalArgumentException.class, () ->
          chaosCanvas.getPixel(point));
    }

    @Test
    @DisplayName("putPixel method throws an exception when point is out of bounds")
    void evaluateChaosCanvasPutPixelThrows() {
      Vector2D point = new Vector2D(2, 2);
      assertThrows(IllegalArgumentException.class, () ->
          chaosCanvas.putPixel(point));
    }
  }


}