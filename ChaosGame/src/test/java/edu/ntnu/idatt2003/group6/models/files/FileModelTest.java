package edu.ntnu.idatt2003.group6.models.files;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

//Disabled since maven struggles to run the files, even though they can run locally
@Disabled
class FileModelTest {

  private FileModel fileModel;
  private ChaosGameDescription chaosGameDescription;

  @BeforeEach
  void setUp() {
    fileModel = FileModel.getInstance();
    chaosGameDescription = ChaosGameDescriptionFactory.
        createChaosGameDescription(GameType.SIERPINSKI);
  }


  @Nested
  @DisplayName("Positive tests for FileModel")
  class methodsDoesNotThrowException {

    @Test
    @DisplayName("getInstance method creates an instance of FileModel without throwing an exception")
    void getInstanceDoesntThrow() {
      assertNotNull(fileModel);
    }

    @Test
    @DisplayName("getFiles method executes without throwing an exception")
    void getFilesDoesntThrow() {
      assertDoesNotThrow(() -> fileModel.getFiles());
    }

    @Test
    @DisplayName("addFile method executes without throwing an exception")
    void addFileDoesntThrow() {
      assertDoesNotThrow(() -> fileModel.addFile("testFile", chaosGameDescription));
    }

    @Test
    @DisplayName("getFile method executes without throwing an exception")
    void getFileDoesntThrow() {
      assertDoesNotThrow(() -> fileModel.getFile("testFile"));
    }

    @Test
    @DisplayName("removeFile method executes without throwing an exception")
    void removeFileDoesntThrow() {
      assertDoesNotThrow(() -> fileModel.removeFile("testFile"));
    }

    @Test
    @DisplayName("setState method executes without throwing an exception")
    void setStateDoesntThrow() {
      assertDoesNotThrow(() -> fileModel.setState(FileState.FILES_CHANGED));
    }

    @Test
    @DisplayName("addObserver method executes without throwing an exception")
    void addObserverDoesntThrow() {
      FileObserver observer = state -> {
        // Do nothing
      };
      assertDoesNotThrow(() -> fileModel.addObserver(observer));
    }

    @Test
    @DisplayName("removeObserver method executes without throwing an exception")
    void removeObserverDoesntThrow() {
      FileObserver observer = state -> {
        // Do nothing
      };
      fileModel.addObserver(observer);
      assertDoesNotThrow(() -> fileModel.removeObserver(observer));
    }
  }

  @Nested
  @DisplayName("Negative tests for FileModel")
  class methodsThrowsExceptions {

    @Test
    @DisplayName("getFiles method throws an exception when file does not exist")
    void getFilesThrowsOnNonExistingFile() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.getFile(null));
    }

    @Test
    @DisplayName("addFile method throws an exception when fileName is null")
    void addFileThrowsOnNullFileName() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.addFile(null, chaosGameDescription));
    }

    @Test
    @DisplayName("addFile method throws an exception when chaosGameDescription is null")
    void addFileThrowsOnNullChaosGameDescription() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.addFile("testFile", null));
    }

    @Test
    @DisplayName("getFile method throws an exception when fileName is null")
    void getFileThrowsOnNullFileName() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.getFile(null));
    }

    @Test
    @DisplayName("getFile method throws an exception when file does not exist")
    void getFileThrowsOnNonExistingFile() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.getFile("nonExistingFile"));
    }

    @Test
    @DisplayName("removeFile method throws an exception when fileName is null")
    void removeFileThrowsOnNullFileName() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.removeFile(null));
    }

    @Test
    @DisplayName("removeFile method throws an exception when file does not exist")
    void removeFileThrowsOnNonExistingFile() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.removeFile("nonExistingFile"));
    }

    @Test
    @DisplayName("setState method throws an exception when state is null")
    void setStateThrowsOnNullState() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.setState(null));
    }

    @Test
    @DisplayName("addObserver method throws an exception when observer is null")
    void addObserverThrowsOnNullObserver() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.addObserver(null));
    }

    @Test
    @DisplayName("removeObserver method throws an exception when observer is null")
    void removeObserverThrowsOnNullObserver() {
      assertThrows(IllegalArgumentException.class, () -> fileModel.removeObserver(null));
    }
  }
}