package edu.ntnu.idatt2003.group6.view.buttonbox;

import javafx.scene.layout.VBox;

/**
 * A class for creating a VBox with buttons for saving,
 * loading and running the game and returning to
 * the menu.
 *
 * @version 0.3.2
 * @see CustomButton
 * @since 0.3.2
 */
public class ButtonBoxControls extends VBox {

  private static final String MENU_BUTTON_STYLE = "menuButton";
  private final CustomButton saveButton;
  private final CustomButton runGameButton;
  private final CustomButton backToMenuButton;
  private final CustomButton loadParametersButton;

  /**
   * Constructor for PlayGameButtonBox.
   * Creates a VBox with text fields for steps, max cords and min cords, and buttons for saving,
   * loading and running the game and returning to the menu.
   */
  public ButtonBoxControls() {

    getStyleClass().add("buttonBox");
    loadParametersButton = new CustomButton("Load Parameters", MENU_BUTTON_STYLE);
    saveButton = new CustomButton("Save Game", MENU_BUTTON_STYLE);
    runGameButton = new CustomButton("Run Game", MENU_BUTTON_STYLE);
    backToMenuButton = new CustomButton("Back to menu", MENU_BUTTON_STYLE);

    getChildren().addAll(
        runGameButton, loadParametersButton, saveButton, backToMenuButton);

  }


  /**
   * Returns the save button.
   *
   * @return the save button.
   */
  public CustomButton getSaveButton() {
    return saveButton;
  }

  /**
   * Returns the run game button.
   *
   * @return the run game button.
   */
  public CustomButton getRunGameButton() {
    return runGameButton;
  }

  /**
   * Returns the back to menu button.
   *
   * @return the back to menu button.
   */
  public CustomButton getBackToMenuButton() {
    return backToMenuButton;
  }

  public CustomButton getLoadParametersButton() {
    return loadParametersButton;
  }
}
