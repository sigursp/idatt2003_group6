package edu.ntnu.idatt2003.group6.view.frames;

import edu.ntnu.idatt2003.group6.view.gamecontrols.AffineControlsView;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AffineTransformationControls;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AttributeControls;
import edu.ntnu.idatt2003.group6.view.gamecontrols.JuliaTransformationControls;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * EditFileFrame is a class that extends the VBox class in JavaFX.
 * It is used to create a frame for editing files with different attributes.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class EditFileFrame extends VBox {
  Label title;
  TextField fileNameField;
  AttributeControls transformationControls;
  JuliaTransformationControls juliaTransformationControls;
  AffineControlsView affineControlsView;
  HBox typeButtons;
  Button juliaTransformButton;
  Button affineTransformButton;
  Button saveButton;

  /**
   * Constructor for EditFileFrame.
   */
  public EditFileFrame() {
    getStylesheets().add("/stylesheets/transformationAttributes.css");
    setAlignment(Pos.CENTER);
    title = new Label("Test");

    Text fileName = new Text("File Name:");
    fileName.getStyleClass().add("attributeNames");
    fileNameField = new TextField();
    fileNameField.setPromptText("Enter new file name");
    fileNameField.getStyleClass().add("textField");
    HBox fileNameBox = new HBox();
    fileNameBox.getChildren().addAll(fileName, fileNameField);

    transformationControls = new AttributeControls();
    transformationControls.removeSteps();

    affineControlsView = new AffineControlsView();
    juliaTransformButton = new Button("Julia Transformation");
    affineTransformButton = new Button("Affine Transformation");
    saveButton = new Button("Save To File");

    typeButtons = new HBox();
    typeButtons.getChildren().addAll(juliaTransformButton, affineTransformButton);
    getChildren().addAll(fileNameBox, transformationControls, typeButtons, saveButton);
  }


  /**
   * Method for clearing the fields in the EditFileFrame.
   */
  public void clearFields() {
    getChildren().remove(2);
    getChildren().add(2, new Pane());
    getChildren().remove(3);
    getChildren().add(3, typeButtons);
  }

  /**
   * Method for setting the file name field in the EditFileFrame.
   *
   * @param fileNameField The file name to be set.
   */
  public void setFileNameField(String fileNameField) {
    this.fileNameField.setText(fileNameField);
  }

  /**
   * Method for setting the transformation controls in the EditFileFrame.
   *
   * @param juliaTransformationControls the julia transformation controls to be set.
   */
  public void setJuliaTransformControls(JuliaTransformationControls juliaTransformationControls) {
    this.juliaTransformationControls = juliaTransformationControls;
    showJuliaAttributes();
  }

  /**
   * method to get the name field of the file.
   *
   * @return the name of the file.
   */
  public String getFileNameField() {
    return fileNameField.getText();
  }

  /**
   * Method to get the common attributes of a transformation.
   *
   * @return the common transformation attributes.
   */
  public AttributeControls getAttributeControls() {
    return transformationControls;
  }

  /**
   * Method to get the julia transformation controls.
   *
   * @return the julia transformation controls.
   */
  public JuliaTransformationControls getJuliaTransformationControls() {
    return juliaTransformationControls;
  }

  /**
   * Method to get the affine transformation controls.
   *
   * @return the affine transformation controls.
   */
  public List<AffineTransformationControls> getAffineTransformControls() {
    return affineControlsView.getAffineControlsList();
  }

  /**
   * Method to get the julia transformation button.
   *
   * @return the julia transformation button.
   */
  public Button getJuliaTransformButton() {
    return juliaTransformButton;
  }

  /**
   * Method to get the affine transformation button.
   *
   * @return the affine transformation button.
   */
  public Button getAffineTransformButton() {
    return affineTransformButton;
  }

  /**
   * Method to get the affine controls view.
   *
   * @return the affine controls view.
   */
  public AffineControlsView getAffineControlsView() {
    return affineControlsView;
  }

  /**
   * Method to get the save button.
   *
   * @return the save button.
   */
  public Button getSaveButton() {
    return saveButton;
  }

  /**
   * Method to show the common transformation attributes from a file.
   */
  public void showFileAttributes() {
    getChildren().clear();
    getChildren().addAll(title, fileNameField, transformationControls, typeButtons, saveButton);
  }

  /**
   * Method to show the affine transformation attributes from a file.
   */
  public void showAffineAttributes() {
    getChildren().clear();
    getChildren().addAll(title, fileNameField, transformationControls,
        affineControlsView.getScrollPaneAffineControls(), saveButton);
  }

  /**
   * Method to show the julia transformation attributes from a file.
   */
  public void showJuliaAttributes() {
    getChildren().clear();
    getChildren().addAll(title, fileNameField, transformationControls,
        juliaTransformationControls, saveButton);
  }

}
