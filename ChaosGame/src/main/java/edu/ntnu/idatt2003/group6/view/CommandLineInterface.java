package edu.ntnu.idatt2003.group6.view;

import static edu.ntnu.idatt2003.group6.utils.ChaosGameFileHandler.numberOfFiles;
import static edu.ntnu.idatt2003.group6.utils.ChaosGameFileHandler.readChaosGameDescriptionFromFile;
import static edu.ntnu.idatt2003.group6.utils.ChaosGameFileHandler.showFiles;
import static edu.ntnu.idatt2003.group6.utils.ChaosGameFileHandler.writeChaosGameDescriptionToFile;
import static edu.ntnu.idatt2003.group6.utils.Utils.editYesNo;
import static edu.ntnu.idatt2003.group6.utils.Utils.inputDouble;
import static edu.ntnu.idatt2003.group6.utils.Utils.inputInt;
import static edu.ntnu.idatt2003.group6.utils.Utils.printListWithNumbers;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGame;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.transformation.AffineTransform2D;
import edu.ntnu.idatt2003.group6.models.transformation.JuliaTransform;
import edu.ntnu.idatt2003.group6.models.transformation.Transform2D;
import edu.ntnu.idatt2003.group6.models.vector.Complex;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents the command line interface for the Chaos Game. It contains methods for
 * interacting with the user through the command line.
 *
 * @version 0.2.5
 * @since 0.2.5
 */
public class CommandLineInterface {
  // Constants representing the different menu choices
  private static final int LOAD_DESCRIPTION_FROM_FILE = 1;
  private static final int WRITE_DESCRIPTION_TO_FILE = 2;
  private static final int RUN_GIVEN_ITERATIONS = 3;
  private static final int PRINT_FRACTAL_TO_CONSOLE = 4;
  private static final int EXIT = 0;
  private static ChaosGame chaosGame;
  private static ChaosGameDescription description;

  /**
   * Main method to run the Chaos Game as a command line application.
   *
   * @param args The command line arguments.
   */
  public static void main(String[] args) {
    CommandLineInterface commandLineInterface = new CommandLineInterface();

    System.out.println("\nWelcome to the Chaos Game!");
    commandLineInterface.interactiveMode();
  }

  private void interactiveMode() {
    boolean finished = false;
    while (!finished) {
      int menuChoice = getMenuChoice();
      switch (menuChoice) {
        case LOAD_DESCRIPTION_FROM_FILE:
          loadDescriptionFromFile();
          break;
        case WRITE_DESCRIPTION_TO_FILE:
          writeDescriptionToFile();
          break;
        case RUN_GIVEN_ITERATIONS:
          runGame();
          break;
        case PRINT_FRACTAL_TO_CONSOLE:
          displayCanvas();
          break;
        case EXIT:
          System.out.println("Thank you for playing the Chaos Game. The program is now Exiting...");
          finished = true;
          break;
        default:
          System.out.println("Invalid choice. Please enter a number between 1 and 4.");
      }
    }
  }

  /**
   * Method to print the main menu.
   */
  private void getMainMenu() {
    System.out.print("""
                
        1. Load description from File
        2. Write description to File
        3. Run Game with set iterations
        4. Print Fractal to Console
        0. Exit

        """);
  }

  /**
   * Method to get the menu choice from the user.
   *
   * @return The menu choice.
   */
  private int getMenuChoice() {
    getMainMenu();
    return inputInt();
  }

  /**
   * Method to run the chaos game for a given number of steps.
   */
  private void runGame() {
    System.out.println("Enter the number of steps to run the chaos game for: ");
    int steps = inputInt();
    try {
      chaosGame.runSteps(steps);
    } catch (Exception e) {
      System.out.println(" Could not run the game: \n" + e.getMessage());
    }
  }

  /**
   * Method to write the description of the chaos game to a file.
   */
  private void writeDescriptionToFile() {
    String file = chooseFile();
    if (file.isEmpty()) {
      System.out.println("No file chosen. Returning to main menu.");
    } else {
      description = readChaosGameDescriptionFromFile(file);
      editCoordinate();

      if (description.getTransformations().getFirst() instanceof AffineTransform2D) {
        writeAffine2dToFile(file);
      } else if (description.getTransformations().getFirst() instanceof JuliaTransform) {
        writeJuliaToFile(file);
      }
    }

  }

  /**
   * Method to load the description of the chaos game from a file.
   */
  private void loadDescriptionFromFile() {
    String file = chooseFile();
    if (file.isEmpty()) {
      System.out.println("No file chosen. Returning to main menu.");
    } else {
      description = readChaosGameDescriptionFromFile(file);
      chaosGame = new ChaosGame(description, 50, 50);
      System.out.println("Chaos Game Description loaded successfully!");
    }
  }

  /**
   * Method to choose a file from the list of files in the descriptions folder.
   *
   * @return The file path of the chosen file.
   */
  public String chooseFile() {
    System.out.println(
        "Which file would you like to choose?"
            + "\n0. Back to main menu");
    printListWithNumbers(showFiles("src/main/resources/descriptions/"));
    int choice = inputInt();
    while (choice < 0 || choice > numberOfFiles("src/main/resources/descriptions/")) {
      System.out.println("Invalid choice. Please enter a number between 0 and "
          + numberOfFiles("src/main/resources/descriptions/"));
      choice = inputInt();
    }
    if (choice == 0) {
      return "";
    } else {
      return "src/main/resources/descriptions/"
          + showFiles("src/main/resources/descriptions/").get(choice - 1);
    }
  }

  /**
   * Method to display the canvas of the chaos game.
   */
  private void displayCanvas() {
    try {
      int[][] canvasArray = chaosGame.getCanvas().getCanvasArray();
      for (int[] row : canvasArray) {
        for (int pixel : row) {
          if (pixel == 0) {
            System.out.print(" ");
          } else if (pixel >= 1) {
            System.out.print("X");
          }
        }
        System.out.println();
      }
    } catch (Exception e) {
      System.out.println("Could not print the canvas: \n" + e.getMessage());
    }
  }

  /**
   * Method to edit the coordinates of the chaos game.
   */
  private void editCoordinate() {
    System.out.println("Current min coordinates: "
        + description.getMinCoords().getX0() + ", " + description.getMinCoords().getX1() + "\n"
        + "Current max coordinates: "
        + description.getMaxCoords().getX0() + ", " + description.getMaxCoords().getX1() + "\n"
        + "Would you like to edit the min and max coordinates? (y/n)");
    if (editYesNo()) {
      System.out.println("Enter the new min coordinates: \n");
      description.setMinCoords(changeVector2D());
      System.out.println("Enter the new max coordinates: \n");
      description.setMaxCoords(changeVector2D());
    }
  }

  /**
   * Method to write the Affine chaos game description to a file.
   */
  private void writeAffine2dToFile(String file) {
    List<Transform2D> newAffineTransforms = new ArrayList<>();
    description.getTransformations().stream()
        .filter(transform -> transform instanceof AffineTransform2D)
        .forEach(transform -> {
          AffineTransform2D affineTransforms = (AffineTransform2D) transform;
          Matrix2x2 matrix = affineTransforms.matrix();
          Vector2D vector = affineTransforms.vector();

          System.out.println("Current matrix: " + matrix.a00() + ", " + matrix.a01()
              + ", " + matrix.a10() + ", " + matrix.a11()
              + "\nWould you like to edit the matrix (y/n)");
          if (editYesNo()) {
            matrix = changeMatrix();
          }
          System.out.println("Current vector: " + vector.getX0() + ", " + vector.getX1()
              + "\nWould you like to edit the vector (y/n)");
          if (editYesNo()) {
            vector = changeVector2D();
          }
          newAffineTransforms.add(new AffineTransform2D(matrix, vector));
        });
    description.setTransformations(newAffineTransforms);
    writeChaosGameDescriptionToFile(
        file, description);
    System.out.println("Description saved to file.");
  }

  /**
   * Method to write the Julia chaos game description to a file.
   */
  private void writeJuliaToFile(String file) {
    System.out.println("Would you like to edit the constant c? (y/n)");
    if (editYesNo()) {
      List<Transform2D> transforms = new ArrayList<>();
      Complex constant = changeComplex();
      transforms.add(new JuliaTransform(constant, 1));
      description.setTransformations(transforms);
    }
    writeChaosGameDescriptionToFile(
        file, description);
    System.out.println("Description saved to file.");
  }

  /**
   * Method to change the vector2D of the chaos game.
   *
   * @return The new vector2D.
   */
  private Vector2D changeVector2D() {
    System.out.println("Enter the x0 value: ");
    double x0 = inputDouble();
    System.out.println("Enter the x1 value: ");
    double x1 = inputDouble();
    return new Vector2D(x0, x1);
  }

  /**
   * Method to change the complex number of the chaos game.
   *
   * @return The new complex number.
   */
  private Complex changeComplex() {
    JuliaTransform descriptionJulia = (JuliaTransform) description.getTransformations().getFirst();
    System.out.println("Enter the real part of the constant c:\n"
        + "Current real part: " + descriptionJulia.getPoint().getX0());
    double x0 = inputDouble();
    System.out.println("Enter the imaginary part of the constant c:\n"
        + "Current imaginary part: " + descriptionJulia.getPoint().getX0());
    double x1 = inputDouble();
    return new Complex(x0, x1);
  }

  /**
   * Method to change the matrix of the chaos game.
   *
   * @return The new matrix.
   */
  private Matrix2x2 changeMatrix() {
    System.out.println("Enter the a00 value: ");
    double a00 = inputDouble();
    System.out.println("Enter the a01 value: ");
    double a01 = inputDouble();
    System.out.println("Enter the a10 value: ");
    double a10 = inputDouble();
    System.out.println("Enter the a11 value: ");
    double a11 = inputDouble();
    return new Matrix2x2(a00, a01, a10, a11);
  }
}
