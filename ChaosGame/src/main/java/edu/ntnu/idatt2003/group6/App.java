package edu.ntnu.idatt2003.group6;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGameController;
import edu.ntnu.idatt2003.group6.controller.navigation.NavigationController;
import edu.ntnu.idatt2003.group6.models.navigation.NavigationModel;
import edu.ntnu.idatt2003.group6.view.frames.HomePage;
import java.util.Objects;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;


/**
 * Main class for the application.
 *
 * @version 0.3.2
 * @since 0.1.1
 */
public class App extends Application {

  /**
   * The main method of the application.
   *
   * @param args The arguments to the main method.
   */
  public static void main(String[] args) {

    launch(args);
  }

  /**
   * The start method of the application.
   *
   * @param primaryStage The primary stage of the application.
   */
  @Override
  public void start(Stage primaryStage) {
    primaryStage.setTitle("Chaos Game");
    primaryStage.setMinWidth(950);
    primaryStage.setMinHeight(600);
    primaryStage.setWidth(1200);
    primaryStage.setHeight(800);
    primaryStage.centerOnScreen();
    primaryStage.getIcons().add(new Image(Objects.requireNonNull(
        ChaosGameController.class.getResourceAsStream("/Logo.png"))));

    HomePage homePage = new HomePage(primaryStage);

    //Creates a new NavigationModel and NavigationController,
    // these acts to change the view of the application.
    NavigationModel navigationModel = new NavigationModel();
    NavigationController.getInstance(homePage, navigationModel);

    Scene scene = new Scene(homePage);
    primaryStage.setScene(scene);
    primaryStage.show();

    //Adds the global stylesheet to the scene.
    scene.getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/stylesheets/globals.css"))
            .toExternalForm());
  }
}

