package edu.ntnu.idatt2003.group6.controller.navigation;

import static edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescriptionFactory.createChaosGameDescription;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGame;
import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGameController;
import edu.ntnu.idatt2003.group6.controller.file.FileController;
import edu.ntnu.idatt2003.group6.controller.file.FileControllerObserver;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.files.FileModel;
import edu.ntnu.idatt2003.group6.models.navigation.NavigationModel;
import edu.ntnu.idatt2003.group6.models.navigation.NavigationState;
import edu.ntnu.idatt2003.group6.view.alert.AlertError;
import edu.ntnu.idatt2003.group6.view.frames.HomePage;

/**
 * The NavigationController class controls the navigation of the application.
 * It changes the view of the application based on the NavigationState.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class NavigationController implements FileControllerObserver {

  private static NavigationController instance = null;
  private final HomePage view;
  private final NavigationModel model;
  @SuppressWarnings("unused")
  private ChaosGameController gameController;
  private final FileModel fileModel = FileModel.getInstance();

  /**
   * Constructor for NavigationController. A singleton class.
   * Create a new NavigationController with a reference to the HomePage and NavigationModel.
   *
   * @param view  The HomePage to bind the navigation to.
   * @param model The NavigationModel to bind the navigation to.
   */
  private NavigationController(HomePage view, NavigationModel model) {
    this.view = view;
    this.model = model;
    model.addObserver(view);

    //Buttons for each of the files are created by the FileController by observing the FileModel
    FileController fileController = FileController.getInstance(view, this);
    fileController.addObserver(this);

    SettingsController.getInstance(view);

    //Buttons to go to different views from the menu
    view.getButtonBoxMenu().getPlayGameButton().setOnAction(e -> this.goToSelectGame());
    view.getButtonBoxMenu().getFilesButton().setOnAction(e -> this.goToFiles());
    view.getButtonBoxMenu().getSettingsButton().setOnAction(e -> this.goToSettings());
    view.getButtonBoxMenu().getQuitButton().setOnAction(e -> System.exit(0));


    //Buttons to go to the menu
    view.getButtonBoxControls().getBackToMenuButton().setOnAction(e -> this.goToMenu());
    view.getButtonBoxFiles().getBackToMenuButton().setOnAction(e -> this.goToMenu());
    view.getButtonBoxSettings().getBackToMenuButton().setOnAction(e -> this.goToMenu());
    view.getButtonBoxGames().getBackToMenuButton().setOnAction(e -> this.goToMenu());

    //Select which game to play
    selectGame();
  }

  /**
   * Returns the instance of the NavigationController.
   * If the instance is null, a new instance is created.
   *
   * @param view  The HomePage to bind the navigation to.
   * @param model The NavigationModel to bind the navigation to.
   * @return The instance of the NavigationController.
   */
  public static NavigationController getInstance(HomePage view, NavigationModel model) {
    if (instance == null) {
      instance = new NavigationController(view, model);
    }
    return instance;
  }

  /**
   * Starts the Affine game with the given ChaosGame.
   *
   * @param game The ChaosGame to start.
   */
  private void startGameAffine(ChaosGame game) {
    gameController = new ChaosGameController(game, view);
    goToPlayAffine();
  }

  /**
   * Starts the Julia game with the given ChaosGame.
   *
   * @param game The ChaosGame to start.
   */
  private void startGameJulia(ChaosGame game) {
    gameController = new ChaosGameController(game, view);
    goToPlayJulia();
  }

  /**
   * Selects which game to play.
   */
  private void selectGame() {
    //Empty Affine game
    view.getButtonBoxGames().getNewAffineButtonButton().setOnAction(e -> this.startGameAffine(
        new ChaosGame(createChaosGameDescription(GameType.AFFINE_NEW), 700, 700)));
    //Empty Julia game
    view.getButtonBoxGames().getNewJuliaButtonButton().setOnAction(e -> this.startGameJulia(
        new ChaosGame(createChaosGameDescription(GameType.JULIA_NEW), 700, 700)));
    //Sierpinski game
    view.getButtonBoxGames().getSierpinskiButton().setOnAction(e -> this.startGameAffine(
        new ChaosGame(createChaosGameDescription(GameType.SIERPINSKI), 700, 700)));
    //Julia game
    view.getButtonBoxGames().getJuliaButton().setOnAction(e -> this.startGameJulia(
        new ChaosGame(createChaosGameDescription(GameType.JULIA), 700, 700)));
    //Barnsley game
    view.getButtonBoxGames().getBarnsleyButton().setOnAction(e -> this.startGameAffine(
        new ChaosGame(createChaosGameDescription(GameType.BARNSLEY), 700, 700)));
    //Show games from file
    view.getButtonBoxGames().getFileButton().setOnAction(e -> this.goToSelectGameFiles());

    selectGameFiles();
  }

  /**
   * Selects the game files to play.
   */
  public void selectGameFiles() {
    //Gets the buttons for each file made by the FileController
    view.getButtonBoxFiles().getFileButtons().forEach(button -> button.setOnAction(e -> {
      try {
        ChaosGameDescription game = fileModel.getFile(button.getText());
        if (game.getGameType().equals(GameType.JULIA)) {
          startGameJulia(new ChaosGame(game, 700, 700));
        } else {
          startGameAffine(new ChaosGame(game, 700, 700));
        }
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    }));
  }

  /**
   * Selects the game files to show.
   */
  private void goToFiles() {
    model.setState(NavigationState.SELECT_FILE);
  }

  /**
   * Allows for editing a file.
   */
  public void goToEditFile() {
    model.setState(NavigationState.EDIT_FILE);
  }

  /**
   * Allows for selecting a game to play.
   */
  private void goToSelectGame() {
    model.setState(NavigationState.SELECT_GAME);
  }

  /**
   * Allows for selecting a game file to play.
   */
  private void goToSelectGameFiles() {
    model.setState(NavigationState.SELECT_GAME_FILES);
  }

  /**
   * Allows for playing the Julia game.
   */
  private void goToPlayJulia() {
    model.setState(NavigationState.PLAY_JULIA);
  }

  /**
   * Allows for playing the Affine game.
   */
  private void goToPlayAffine() {
    model.setState(NavigationState.PLAY_AFFINE);
  }

  /**
   * Allows for going to the settings.
   */
  private void goToSettings() {
    model.setState(NavigationState.SETTINGS);
  }

  /**
   * Allows for going to the menu.
   */
  private void goToMenu() {
    model.setState(NavigationState.MENU);
  }

  /**
   * Updates the view to show the files in the file directory. With the buttons for each file.
   */
  @Override
  public void update() {
    selectGameFiles();
  }
}
