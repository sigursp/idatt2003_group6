package edu.ntnu.idatt2003.group6.utils;

import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.transformation.AffineTransform2D;
import edu.ntnu.idatt2003.group6.models.transformation.JuliaTransform;
import edu.ntnu.idatt2003.group6.models.transformation.Transform2D;
import edu.ntnu.idatt2003.group6.models.vector.Complex;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * This class contains methods to handle reading and
 * writing ChaosGameDescriptions to and from files.
 *
 * @version 0.3.2
 * @since 0.2.4
 */
public class ChaosGameFileHandler {

  private static final Logger LOGGER = Logger.getLogger(ChaosGameFileHandler.class.getName());

  /**
   * Method to write to file.
   *
   * @param fileName name of file
   * @param content  content to write to file
   */
  static void writeToFile(String fileName, String content) {
    try (FileWriter fileWriter = new FileWriter(fileName)) {
      fileWriter.write(content);
    } catch (IOException e) {
      LOGGER.severe("An error occurred while writing to file: " + e.getMessage());
      throw new RuntimeException("An error occurred while writing to file: " + e.getMessage());
    }
  }

  /**
   * Method to write extra lines to file.
   *
   * @param fileName name of file
   * @param content  content to be added to the file
   *                 without overwriting the existing content in the file.
   * @throws RuntimeException if an error occurs while writing to file.
   */
  static void writeToFileExtra(String fileName, String content) throws RuntimeException {
    try (FileWriter fileWriter = new FileWriter(fileName, true)) {
      fileWriter.write(content);
    } catch (IOException e) {
      LOGGER.severe("An error occurred while writing to file: " + e.getMessage());
      throw new RuntimeException("An error occurred while writing to file: " + e.getMessage());
    }
  }

  /**
   * Method to read from a file.
   *
   * @param fileName name of file
   * @return content of file
   * @throws RuntimeException if an error occurs while reading from file
   */
  static String readFromFile(String fileName) throws RuntimeException {
    try {
      return Files.readString(Paths.get(fileName));
    } catch (IOException e) {
      LOGGER.severe("An error occurred while reading from file: " + e.getMessage());
      throw new RuntimeException("An error occurred while reading from file: " + e.getMessage(), e);
    }
  }

  /**
   * Method to write a ChaosGameDescription to a file.
   *
   * @param fileName             name of file to write to
   * @param chaosGameDescription the ChaosGameDescription to write to file
   * @throws RuntimeException if an error occurs while writing to file
   */
  public static void writeChaosGameDescriptionToFile(
      String fileName, ChaosGameDescription chaosGameDescription) throws RuntimeException {
    try {

      /* Write the first line. Type of transformation */
      if (chaosGameDescription.getTransformations().getFirst().getClass()
          == AffineTransform2D.class) {
        writeToFile(fileName, "Affine2D                # Type of transform \n");
      } else if (chaosGameDescription.getTransformations().getFirst().getClass()
          == JuliaTransform.class) {
        writeToFile(fileName, "Julia               # Type of transform \n");
      }

      /* Write the second line. Coordinates of the lower left corner of the canvas */
      writeToFileExtra(fileName,
          chaosGameDescription.getMinCoords().getX0() + ", "
              + chaosGameDescription.getMinCoords().getX1() + " # Lower left" + "\n");

      /* Write the third line. The Coordinates in the upper right corner of the canvas */
      writeToFileExtra(fileName,
          chaosGameDescription.getMaxCoords().getX0() + ", "
              + chaosGameDescription.getMaxCoords().getX1() + " # Upper right" + "\n");

      /* Write fourth line++.
      A line for each transformation With all parameters separated by a comma. */
      List<Transform2D> transformations = chaosGameDescription.getTransformations();
      for (int i = 0; i < transformations.size(); i++) {
        if (transformations.get(i) instanceof AffineTransform2D affineTransform2D) {
          writeToFileExtra(fileName,
              affineTransform2D.matrix().a00()
                  + ", " + affineTransform2D.matrix().a01()
                  + ", " + affineTransform2D.matrix().a10()
                  + ", " + affineTransform2D.matrix().a11()
                  + ", " + affineTransform2D.vector().getX0()
                  + ", " + affineTransform2D.vector().getX1()
                  + " # " + (i + 1) + "nd transform" + "\n");
        } else if (transformations.get(i) instanceof JuliaTransform juliaTransform) {
          writeToFileExtra(fileName,
              juliaTransform.getPoint().getX0() + ", "
                  + juliaTransform.getPoint().getX1() + ", "
                  + " # Real and imaginary part of the constant c");
          return;
        }
      }
    } catch (Exception e) {
      LOGGER.severe("An error occurred while writing ChaosGameDescription to file: "
          + e.getMessage());
      throw new RuntimeException("An error occurred while writing ChaosGameDescription to file: "
          + e.getMessage(), e);
    }
  }

  /**
   * Method to read a ChaosGameDescription from a file.
   *
   * @param fileName name of file to read from
   * @return ChaosGameDescription read from file
   * @throws RuntimeException if an error occurs while reading from file
   */
  public static ChaosGameDescription readChaosGameDescriptionFromFile(String fileName)
      throws RuntimeException {
    try {
      String content = readFromFile(fileName);
      String[] lines = content.split("\n");

      String type = lines[0].split("#")[0].trim();
      Vector2D minCoords = parseVector2D(lines[1]);
      Vector2D maxCoords = parseVector2D(lines[2]);

      List<Transform2D> transformList = new ArrayList<>();
      for (int i = 3; i < lines.length; i++) {
        String transformation = lines[i].split("#")[0].trim();
        if (type.equals("Affine2D")) {
          transformList.add(parseAffineTransform2D(transformation));
        } else if (type.equals("Julia")) {
          transformList.add(parseJuliaTransform(transformation, 1));
          transformList.add(parseJuliaTransform(transformation, -1));
        }
      }
      return new ChaosGameDescription(transformList, minCoords, maxCoords);
    } catch (Exception e) {
      LOGGER.severe("An error occurred while reading ChaosGameDescription from file: "
          + e.getMessage());
      throw new RuntimeException("An error occurred while reading ChaosGameDescription from file: "
          + e.getMessage(), e);
    }
  }

  /**
   * Method to parse a string to a Vector2D object.
   *
   * @param line The string to parse.
   * @return A Vector2D object.
   */
  private static Vector2D parseVector2D(String line) {
    String[] coords = line.split("#")[0].split(",");
    double x0 = Double.parseDouble(coords[0].trim());
    double x1 = Double.parseDouble(coords[1].trim());
    return new Vector2D(x0, x1);
  }

  /**
   * Method to parse a string to an AffineTransform2D object.
   *
   * @param transformation The string to parse.
   * @return An AffineTransform2D object.
   */
  private static AffineTransform2D parseAffineTransform2D(String transformation) {
    String[] params = transformation.split(",");
    double a00 = Double.parseDouble(params[0].trim());
    double a01 = Double.parseDouble(params[1].trim());
    double a10 = Double.parseDouble(params[2].trim());
    double a11 = Double.parseDouble(params[3].trim());
    double x0 = Double.parseDouble(params[4].trim());
    double x1 = Double.parseDouble(params[5].trim());
    Matrix2x2 matrix = new Matrix2x2(a00, a01, a10, a11);
    Vector2D vector = new Vector2D(x0, x1);
    return new AffineTransform2D(matrix, vector);
  }

  /**
   * Method to parse a string to a JuliaTransform object.
   *
   * @param transformation The string to parse.
   * @param sign           The sign of the imaginary part of the complex number.
   * @return A JuliaTransform object.
   */
  private static JuliaTransform parseJuliaTransform(String transformation, int sign) {
    String[] params = transformation.split(",");
    double x0 = Double.parseDouble(params[0].trim());
    double x1 = Double.parseDouble(params[1].trim());
    Complex complex = new Complex(x0, x1);
    return new JuliaTransform(complex, sign);
  }

  /**
   * Method to show files in a directory.
   *
   * @param directoryPath The path to the directory.
   * @return A list of strings with the name of files in the directory.
   * @throws RuntimeException if an error occurs while showing files.
   */
  public static List<String> showFiles(String directoryPath) throws RuntimeException {
    Path path = Paths.get(directoryPath);

    try (var stream = Files.walk(path)) {
      return stream
          .filter(Files::isRegularFile)
          .map(filePath -> filePath.getFileName().toString())
          .collect(Collectors.toList());
    } catch (IOException e) {
      LOGGER.severe("An error occurred while showing files: " + e.getMessage());
      throw new RuntimeException("An error occurred while showing files: " + e.getMessage());
    }
  }

  /**
   * Method to count the number of files in a directory.
   *
   * @param directoryPath The path to the directory.
   * @return The number of files in the directory.
   * @throws RuntimeException if an error occurs while counting files.
   */
  public static int numberOfFiles(String directoryPath) throws RuntimeException {
    Path path = Paths.get(directoryPath);
    try (var stream = Files.walk(path)) {
      return (int) stream
          .filter(Files::isRegularFile)
          .count();
    } catch (IOException e) {
      LOGGER.severe("An error occurred while counting files: " + e.getMessage());
      throw new RuntimeException("An error occurred while counting files: " + e.getMessage());
    }
  }

  /**
   * Method to delete a file from the resource folder.
   *
   * @param path The name of the file to be deleted.
   *             The file must be in the resource folder.
   * @throws RuntimeException if an error occurs while deleting the file.
   */
  public static void deleteFile(String path) throws RuntimeException {
    try {
      Files.delete(Paths.get(path));
    } catch (IOException e) {
      LOGGER.severe("An error occurred while deleting file: " + e.getMessage());
      throw new RuntimeException("An error occurred while deleting file: " + e.getMessage());
    }
  }
}
