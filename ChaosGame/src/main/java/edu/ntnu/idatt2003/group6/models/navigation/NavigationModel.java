package edu.ntnu.idatt2003.group6.models.navigation;

import edu.ntnu.idatt2003.group6.controller.navigation.NavigationController;
import java.util.ArrayList;
import java.util.List;

/**
 * The NavigationModel class manages the
 * state of the navigation in the application.
 * It provides methods for setting and getting the state
 * and for adding and removing observers.
 * The observers are notified when the state changes.
 * The class is part of the Model-View-Controller pattern.
 *
 * @version 0.3.2
 * @see NavigationState
 * @see NavigationObserver
 * @see NavigationController
 * @since 0.3.2
 */
public class NavigationModel {
  private final List<NavigationObserver> observers = new ArrayList<>();
  private NavigationState state;

  /**
   * Returns the current state of the navigation.
   */
  public NavigationState getState() {
    return state;
  }

  /**
   * Sets the state of the navigation.
   *
   * @param state the new state of the navigation.
   * @throws IllegalArgumentException if the state is null.
   */
  public void setState(NavigationState state) throws IllegalArgumentException {
    if (state == null) {
      throw new IllegalArgumentException("State cannot be null");
    }
    this.state = state;
    notifyObservers();
  }

  /**
   * Adds an observer to the navigation model.
   *
   * @param observer the observer to be added.
   * @throws IllegalArgumentException if the observer is null.
   */
  public void addObserver(NavigationObserver observer) throws IllegalArgumentException {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null");
    }
    observers.add(observer);
  }

  /**
   * Removes an observer from the navigation model.
   *
   * @param observer the observer to be removed.
   * @throws IllegalArgumentException if the observer is null.
   */
  public void removeObserver(NavigationObserver observer) throws IllegalArgumentException {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null");
    }
    observers.remove(observer);
  }

  /**
   * Notifies all observers that the state has changed.
   */
  private void notifyObservers() {
    observers.forEach(observer -> observer.update(state));
  }

}
