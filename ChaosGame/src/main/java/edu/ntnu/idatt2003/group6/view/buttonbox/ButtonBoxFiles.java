package edu.ntnu.idatt2003.group6.view.buttonbox;

import java.util.List;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBox;

/**
 * A class for creating a VBox with buttons for editing,
 * creating and deleting files and returning to the menu.
 *
 * @version 0.3.2
 * @see CustomButton
 * @see CustomToggleButton
 * @since 0.3.2
 */
public class ButtonBoxFiles extends VBox {

  private static final String MENU_BUTTON_STYLE = "menuButton";

  private final VBox filesButtonsBoxToggle;
  private final VBox filesButtonsBox;

  private final CustomButton newFileButton;
  private final CustomButton editFileButton;
  private final CustomButton deleteFileButton;
  private final CustomButton backToMenuButton;
  private final ToggleGroup toggleButtons;

  /**
   * Constructor for PlayGameButtonBox.
   * Creates a VBox with text fields for steps, max cords and min cords, and buttons for saving,
   * loading and running the game and returning to the menu.
   */
  public ButtonBoxFiles() {

    toggleButtons = new ToggleGroup();
    filesButtonsBoxToggle = new VBox();
    filesButtonsBox = new VBox();

    getStyleClass().add("buttonBox");

    editFileButton = new CustomButton("Edit file", MENU_BUTTON_STYLE);
    newFileButton = new CustomButton("New file", MENU_BUTTON_STYLE);
    deleteFileButton = new CustomButton("Delete file", MENU_BUTTON_STYLE);
    backToMenuButton = new CustomButton("Back to menu", MENU_BUTTON_STYLE);

    VBox menuButtonsBox = new VBox();
    menuButtonsBox.getChildren().addAll(editFileButton, newFileButton, deleteFileButton,
        backToMenuButton);
    setSpacing(20);
    getChildren().addAll(filesButtonsBoxToggle, menuButtonsBox);
  }

  public VBox getFilesButtonsBox() {
    return filesButtonsBox;
  }

  public List<CustomToggleButton> getFileButtonsToggle() {
    return filesButtonsBoxToggle.getChildren().stream()
        .map(e -> (CustomToggleButton) e).toList();
  }

  public List<CustomButton> getFileButtons() {
    return filesButtonsBox.getChildren().stream()
        .map(e -> (CustomButton) e).toList();
  }

  public void addFileButton(String filename) {
    addFileButtonToggleGroup(filename);
    addFileButtonCustom(filename);
  }

  private void addFileButtonToggleGroup(String filename) {
    CustomToggleButton button = new CustomToggleButton(filename, MENU_BUTTON_STYLE);
    filesButtonsBoxToggle.getChildren().add(button);
    button.setToggleGroup(toggleButtons);
  }

  private void addFileButtonCustom(String filename) {
    CustomButton button = new CustomButton(filename, MENU_BUTTON_STYLE);
    filesButtonsBox.getChildren().add(button);
  }


  public void clearFileButtons() {
    filesButtonsBoxToggle.getChildren().remove(0, filesButtonsBoxToggle.getChildren().size());
    filesButtonsBox.getChildren().remove(0, filesButtonsBox.getChildren().size());
  }

  public ToggleGroup getToggleButtons() {
    return toggleButtons;
  }

  /**
   * Returns the back to menu button.
   *
   * @return the back to menu button.
   */
  public CustomButton getBackToMenuButton() {
    return backToMenuButton;
  }

  public CustomButton getEditFileButton() {
    return editFileButton;
  }

  public CustomButton getDeleteFileButton() {
    return deleteFileButton;
  }

  public CustomButton getNewFileButton() {
    return newFileButton;
  }

}
