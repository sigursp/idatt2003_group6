package edu.ntnu.idatt2003.group6.view.gamecontrols;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGameController;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * A class that represents the common attributes for the transformations in the Chaos Game.
 * The class extends GridPane and contains TextFields for the steps and the min and max coordinates.
 * The class also contains a Text object for the attribute names and a Label for error messages.
 * The class is used for both the affine and julia transformations.
 *
 * @version 0.3.2
 * @see AffineTransformationControls
 * @see JuliaTransformationControls
 * @see ChaosGameController
 * @see edu.ntnu.idatt2003.group6.view.gamecontrols.JuliaTransformationControls
 * @see edu.ntnu.idatt2003.group6.view.gamecontrols.AffineTransformationControls
 * @since 0.3.2
 */
public class AttributeControls extends GridPane {

  private final Text steps;
  private final TextField stepsField;
  private final TextField minCoordsX0Field;
  private final TextField minCoordsX1Field;
  private final TextField maxCoordsX0Field;
  private final TextField maxCoordsX1Field;
  private final Label stepsErrorLabel;

  /**
   * Constructor for the AttributeControls class.
   * Initializes the TextFields and Labels for the steps and the min and max coordinates.
   * Also sets the style classes for the different nodes.
   */
  public AttributeControls() {
    getStylesheets().add("/stylesheets/transformationAttributes.css");
    getStyleClass().add("frame");

    steps = new Text("Steps:");
    stepsField = new TextField();
    minCoordsX0Field = new TextField();
    minCoordsX1Field = new TextField();
    maxCoordsX0Field = new TextField();
    maxCoordsX1Field = new TextField();

    stepsField.setPromptText("Enter steps");
    minCoordsX0Field.setPromptText("0.0");
    minCoordsX1Field.setPromptText("0.0");
    maxCoordsX0Field.setPromptText("0.0");
    maxCoordsX1Field.setPromptText("0.0");

    stepsErrorLabel = new Label();



    add(steps, 0, 0);
    add(stepsField, 1, 0);
    add(stepsErrorLabel, 0, 1);

    Text minCords = new Text("Min Cords:");
    add(minCords, 0, 2);
    add(minCoordsX0Field, 1, 2);
    add(minCoordsX1Field, 2, 2);

    Label minCordsErrorLabel = new Label();
    add(minCordsErrorLabel, 0, 3);

    Text maxCords = new Text("Max Cords:");
    add(maxCords, 0, 4);
    add(maxCoordsX0Field, 1, 4);
    add(maxCoordsX1Field, 2, 4);

    Label maxCordsErrorLabel = new Label();
    add(maxCordsErrorLabel, 0, 5);

    setColumnSpan(stepsErrorLabel, 3);
    setColumnSpan(minCordsErrorLabel, 3);
    setColumnSpan(maxCordsErrorLabel, 3);

    List<Node> children = getChildren();
    for (Node node : children) {
      if (node instanceof Text) {
        node.getStyleClass().add("attributeNames");
      } else if (node instanceof TextField) {
        node.getStyleClass().add("textField");
      } else if (node instanceof Label) {
        node.getStyleClass().add("errorLabel");
      }
    }
  }

  /**
   * Returns the steps TextField.
   *
   * @return The steps TextField.
   */
  public String getStepsField() {
    return stepsField.getText();
  }

  /**
   * gets the minimum coords for x0.
   *
   * @return the minimum coords for x0.
   */
  public String getMinCoordsX0Field() {
    return minCoordsX0Field.getText();
  }

  /**
   * gets the minimum coords for x1.
   *
   * @return the minimum coords for x1.
   */
  public String getMinCoordsX1Field() {
    return minCoordsX1Field.getText();
  }

  /**
   * gets the maximum coords for x0.
   *
   * @return the maximum coords for x0.
   */
  public String getMaxCoordsX0Field() {
    return maxCoordsX0Field.getText();
  }

  /**
   * gets the maximum coords for x1.
   *
   * @return the maximum coords for x1.
   */
  public String getMaxCoordsX1Field() {
    return maxCoordsX1Field.getText();
  }

  /**
   * sets the minimum coords for x0.
   *
   * @param minCoordsX0 the minimum coords for x0.
   */
  public void setMinCoordsX0Field(String minCoordsX0) {
    minCoordsX0Field.setText(minCoordsX0);
  }

  /**
   * sets the minimum coords for x1.
   *
   * @param minCoordsX1 the minimum coords for x1.
   */
  public void setMinCoordsX1Field(String minCoordsX1) {
    minCoordsX1Field.setText(minCoordsX1);
  }

  /**
   * sets the maximum coords for x0.
   *
   * @param maxCoordsX0 the maximum coords for x0.
   */
  public void setMaxCoordsX0Field(String maxCoordsX0) {
    maxCoordsX0Field.setText(maxCoordsX0);
  }

  /**
   * sets the maximum coords for x1.
   *
   * @param maxCoordsX1 the maximum coords for x1.
   */
  public void setMaxCoordsX1Field(String maxCoordsX1) {
    maxCoordsX1Field.setText(maxCoordsX1);
  }

  /**
   * removes the steps from the attribute controls.
   */
  public void removeSteps() {
    getChildren().remove(steps);
    getChildren().remove(stepsField);
    getChildren().remove(stepsErrorLabel);
  }
}
