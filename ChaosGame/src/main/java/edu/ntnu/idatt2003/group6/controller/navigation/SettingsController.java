package edu.ntnu.idatt2003.group6.controller.navigation;

import edu.ntnu.idatt2003.group6.models.files.FilePath;
import edu.ntnu.idatt2003.group6.view.frames.HomePage;
import edu.ntnu.idatt2003.group6.view.frames.SettingsFrame;
import java.io.File;
import javafx.scene.control.ToggleGroup;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * The SettingsController class controls the settings of the application.
 * Mainly the music settings.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class SettingsController {
  private static SettingsController instance = null;
  private final ToggleGroup musicOption;
  private final SettingsFrame settingsFrame;
  private final MediaPlayer mediaPlayer;

  /**
   * Constructor for SettingsController.
   * Creates a new SettingsController with a reference to the HomePage.
   *
   * @param homePage The HomePage to bind the settings to.
   */
  private SettingsController(HomePage homePage) {
    this.settingsFrame = homePage.getSettingsFrame();
    this.musicOption = settingsFrame.getMusicOption();

    Media media =
        new Media(new File(
            FilePath.MUSIC.getPath() + "Legio_Symphonica.mp3").toURI().toString());
    mediaPlayer = new MediaPlayer(media);
    mediaPlayer.setCycleCount(MediaPlayer.INDEFINITE);

    musicOption.selectToggle(settingsFrame.getMusicOff());
    buttonsController();
  }

  /**
   * Returns the instance of the SettingsController.
   * If the instance is null, a new instance is created.
   *
   * @param homepage The HomePage to bind the settings to.
   * @return The instance of the SettingsController.
   */
  public static SettingsController getInstance(HomePage homepage) {
    if (instance == null) {
      instance = new SettingsController(homepage);
    }
    return instance;
  }

  /**
   * Controls the music settings.
   * If the music is turned off, the mediaPlayer is stopped.
   * If the music is turned on, the mediaPlayer is played.
   * The volume of the mediaPlayer is set to the value of the volumeSlider.
   */
  private void buttonsController() {
    musicOption.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
      if (musicOption.getSelectedToggle() == settingsFrame.getMusicOn()) {
        mediaPlayer.play();
        mediaPlayer.setVolume(settingsFrame.getVolumeSlider().getValue());
      } else if (musicOption.getSelectedToggle() == settingsFrame.getMusicOff()) {
        mediaPlayer.stop();
      }
    });

    settingsFrame.getVolumeSlider().valueProperty().addListener((observable, oldValue, newValue) ->
        mediaPlayer.setVolume(settingsFrame.getVolumeSlider().getValue()));
  }
}
