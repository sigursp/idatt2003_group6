package edu.ntnu.idatt2003.group6.models.chaosgame;

import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.transformation.AffineTransform2D;
import edu.ntnu.idatt2003.group6.models.transformation.JuliaTransform;
import edu.ntnu.idatt2003.group6.models.vector.Complex;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import java.util.List;

/**
 * A class that represents a factory for creating standard ChaosGameDescriptions.
 *
 * @version 0.3.1
 * @since 0.3.1
 */
public class ChaosGameDescriptionFactory {

  /**
   * Creates a standard ChaosGameDescription based on the game type provided.
   *
   * @param gameType The type of chaos game to create
   * @return A ChaosGameDescription of the type you want
   * @throws IllegalArgumentException If the game type is null or invalid
   */
  public static ChaosGameDescription createChaosGameDescription(GameType gameType)
      throws IllegalArgumentException {
    try {
      return switch (gameType) {
        case SIERPINSKI -> new ChaosGameDescription(List.of(
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0, 0)),
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.25, 0.5)),
            new AffineTransform2D(new Matrix2x2(0.5, 0, 0, 0.5), new Vector2D(0.5, 0))
        ), new Vector2D(0, 0), new Vector2D(1, 1));

        case BARNSLEY -> new ChaosGameDescription(List.of(
            new AffineTransform2D(new Matrix2x2(0, 0, 0, 0.16), new Vector2D(0, 0)),
            new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85), new Vector2D(0, 1.6)),
            new AffineTransform2D(new Matrix2x2(0.2, -0.26, 0.23, 0.22), new Vector2D(0, 1.6)),
            new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24), new Vector2D(0, 0.44))
        ), new Vector2D(-2.65, 0), new Vector2D(2.65, 10));

        case JULIA -> new ChaosGameDescription(List.of(
            new JuliaTransform(new Complex(-0.74543, 0.11301), 1),
            new JuliaTransform(new Complex(-0.74543, 0.11301), -1)
        ), new Vector2D(-1.6, -1), new Vector2D(1.6, 1));

        case JULIA_NEW -> new ChaosGameDescription(List.of(
            new JuliaTransform(new Complex(0, 0), -1),
            new JuliaTransform(new Complex(0, 0), 1)
        ), new Vector2D(0, 0), new Vector2D(0, 0));

        case AFFINE_NEW -> new ChaosGameDescription(List.of(
            new AffineTransform2D(new Matrix2x2(0, 0, 0, 0), new Vector2D(0, 0))
        ), new Vector2D(0, 0), new Vector2D(0, 0));
        default -> null;
      };
    } catch (NullPointerException e) {
      throw new IllegalArgumentException("Game type cannot be null. @" + e.getMessage(), e);
    }
  }
}
