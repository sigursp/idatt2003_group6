package edu.ntnu.idatt2003.group6.models.files;

/**
 * Enum class for the file states used in the FileModel class.
 * The states are FILES_UPDATING, FILES_UNCHANGED and FILES_CHANGED.
 * FILES_UPDATING is used when the files are being updated.
 * FILES_UNCHANGED is used when the files are unchanged.
 * FILES_CHANGED is used when the files are changed.
 *
 * @version 0.3.2
 * @see edu.ntnu.idatt2003.group6.models.files.FileModel
 * @since 0.3.2
 */
public enum FileState {
  FILES_UPDATING,
  FILES_UNCHANGED,
  FILES_CHANGED

}
