package edu.ntnu.idatt2003.group6.models.chaosgame;

import edu.ntnu.idatt2003.group6.models.transformation.Transform2D;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import edu.ntnu.idatt2003.group6.utils.exceptions.IllegalChaosGameException;
import java.util.List;

/**
 * A class that represents a description of a chaos game.
 * It contains a list of transformations and the minimum and maximum coordinates of the chaos game.
 *
 * @version 0.2.1
 * @since 0.2.1
 */
public class ChaosGameDescription {
  private final GameType gameType;
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private List<Transform2D> transformations;

  /**
   * Constructor for the ChaosGameDescription class.
   *
   * @param transforms A list of transformations
   * @param minCoords  start coordinates
   * @param maxCoords  stop coordinates
   * @throws IllegalChaosGameException if the transformations, minCoords or maxCoords are null
   */
  public ChaosGameDescription(List<Transform2D> transforms,
                              Vector2D minCoords, Vector2D maxCoords)
      throws IllegalChaosGameException {
    if (transforms == null || minCoords == null || maxCoords == null) {
      throw new IllegalChaosGameException(
          "Transformations, minCoords or maxCoords cannot be null.");
    }
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transformations = transforms;
    this.gameType = transformations.getFirst().gameType();
  }

  /**
   * Returns the list of transformations.
   *
   * @return A list of transformations
   */
  public List<Transform2D> getTransformations() {
    return transformations;
  }

  /**
   * Sets new transformations of the chaos game.
   *
   * @param transformations A list of transformations
   */
  public void setTransformations(List<Transform2D> transformations) {
    this.transformations = transformations;
  }

  /**
   * Returns the minimum coordinates of the chaos game.
   *
   * @return The minimum coordinates
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Sets new minimum coordinates of the chaos game.
   *
   * @param minCoords The minimum coordinates
   */
  public void setMinCoords(Vector2D minCoords) {
    this.minCoords = minCoords;
  }

  /**
   * Returns the maximum coordinates of the chaos game.
   *
   * @return The maximum coordinates
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  /**
   * Sets new maximum coordinates of the chaos game.
   *
   * @param maxCoords The maximum coordinates
   */
  public void setMaxCoords(Vector2D maxCoords) {
    this.maxCoords = maxCoords;
  }

  public GameType getGameType() {
    return gameType;
  }
}
