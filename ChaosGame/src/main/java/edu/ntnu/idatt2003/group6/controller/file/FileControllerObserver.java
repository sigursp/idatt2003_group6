package edu.ntnu.idatt2003.group6.controller.file;

/**
 * Interface for the FileControllerObserver class.
 */
public interface FileControllerObserver {
  void update();
}
