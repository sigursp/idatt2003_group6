package edu.ntnu.idatt2003.group6.models.transformation;

import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;

/**
 * This interface represents a 2D transformation.
 * It contains an abstract method to transform a 2D vector.
 *
 * @version 0.1.3
 * @see AffineTransform2D
 * @see JuliaTransform
 * @since 0.1.3
 */
public interface Transform2D {

  /**
   * Transforms the given vector by this transformation.
   *
   * @param vector The vector to be transformed by this transformation.
   * @return A new vector that is the result of the transformation.
   */
  Vector2D transform(Vector2D vector);

  /**
   * Returns the type of game that this transformation is used for.
   *
   * @return The type of game that this transformation is used for.
   */
  GameType gameType();
}
