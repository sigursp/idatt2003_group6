package edu.ntnu.idatt2003.group6.controller.chaosgame;

/**
 * An enum representing the state of the game.
 * Gives a reference to the current state of the game to decide further actions.
 *
 * @version 3.2
 * @since 3.2
 */
public enum GameState {
  RUNNING,
  DONE
}
