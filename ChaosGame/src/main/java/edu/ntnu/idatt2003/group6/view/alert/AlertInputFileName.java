package edu.ntnu.idatt2003.group6.view.alert;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGameController;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Alert for inputting a file name.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class AlertInputFileName extends Alert {
  private final TextField fileName;

  /**
   * Constructor for the AlertInputFileName class.
   */
  public AlertInputFileName() {
    super(AlertType.CONFIRMATION);
    this.getDialogPane().getStylesheets().add(
        Objects.requireNonNull(getClass().getResource(
            "/stylesheets/alert.css")).toExternalForm());

    setTitle("Enter file name");
    setHeaderText(null);
    setGraphic(null);
    setIcon();

    VBox content = new VBox();
    content.setPadding(new Insets(10));
    content.setSpacing(10);
    Text text = new Text("Please enter the name of the file you want to save: ");
    fileName = new TextField();
    text.getStyleClass().add("informationText");
    fileName.getStyleClass().add("textField");
    content.getChildren().addAll(text, fileName);
    getDialogPane().setContent(content);

    getDialogPane().lookupButton(ButtonType.OK).getStyleClass().add("positiveButton");
    getDialogPane().lookupButton(ButtonType.CANCEL).getStyleClass().add("negativeButton");
  }


  /**
   * Returns the file name.
   *
   * @return the file name.
   */
  public String getFileName() {
    return fileName.getText();
  }

  /**
   * Shows the alert.
   */
  public void showAlert() {
    this.showAndWait();
  }

  /**
   * Sets the icon of the alert.
   */
  private void setIcon() {
    ((Stage) this.getDialogPane().getScene().getWindow()).getIcons()
        .add(new Image(Objects.requireNonNull(
            ChaosGameController.class.getResourceAsStream("/Logo.png"))));
  }
}
