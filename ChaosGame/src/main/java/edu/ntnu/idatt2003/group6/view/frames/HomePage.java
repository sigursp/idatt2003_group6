package edu.ntnu.idatt2003.group6.view.frames;


import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGame;
import edu.ntnu.idatt2003.group6.models.navigation.NavigationObserver;
import edu.ntnu.idatt2003.group6.models.navigation.NavigationState;
import edu.ntnu.idatt2003.group6.view.buttonbox.ButtonBoxControls;
import edu.ntnu.idatt2003.group6.view.buttonbox.ButtonBoxFiles;
import edu.ntnu.idatt2003.group6.view.buttonbox.ButtonBoxGames;
import edu.ntnu.idatt2003.group6.view.buttonbox.ButtonBoxMenu;
import edu.ntnu.idatt2003.group6.view.buttonbox.ButtonBoxSettings;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AffineControlsView;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AttributeControls;
import edu.ntnu.idatt2003.group6.view.gamecontrols.JuliaTransformationControls;
import java.util.Objects;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


/**
 * The HomePage class is the main view of the application.
 * It contains the menu buttons and the content frame.
 * The content frame is where the different views are displayed.
 * The views are controlled by the NavigationState enum.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class HomePage extends StackPane implements NavigationObserver {
  private final Pane contentFrame;
  private final ButtonBoxMenu buttonBoxMenu;
  private final BorderPane buttonBoxFrame;
  private final ButtonBoxControls buttonBoxControls;
  private final ButtonBoxFiles buttonBoxFiles;
  private final ButtonBoxSettings buttonBoxSettings;
  private final TransformationFrame transformationFrame;
  private final ButtonBoxGames buttonBoxSelectGame;
  private final AttributeControls attributeControls;
  private JuliaTransformationControls juliaTransformationControls;
  private final AffineControlsView affineControlsView;
  private final EditFileFrame editFileFrame;
  private final SettingsFrame settingsFrame;
  private final ImageView logo;

  /**
   * Creates an instance of the HomePage class.
   *
   * @param stage The stage to bind the width and height properties to.
   */
  public HomePage(Stage stage) {

    buttonBoxMenu = new ButtonBoxMenu();
    buttonBoxFrame = new BorderPane();
    contentFrame = new Pane();
    buttonBoxControls = new ButtonBoxControls();
    buttonBoxFiles = new ButtonBoxFiles();
    buttonBoxSettings = new ButtonBoxSettings();
    transformationFrame = new TransformationFrame();
    buttonBoxSelectGame = new ButtonBoxGames();
    editFileFrame = new EditFileFrame();
    settingsFrame = new SettingsFrame();

    attributeControls = new AttributeControls();
    affineControlsView = new AffineControlsView();
    juliaTransformationControls = new JuliaTransformationControls();

    this.prefWidthProperty().bind(stage.widthProperty());
    this.prefHeightProperty().bind(stage.heightProperty());
    getStyleClass().add("homePage");
    Image image = new Image(
        Objects.requireNonNull(getClass().getResourceAsStream("/Logo.png")));

    logo = new ImageView(image);
    logo.opacityProperty().setValue(0.5);
    logo.fitWidthProperty().bind(contentFrame.widthProperty());
    logo.fitHeightProperty().bind(contentFrame.heightProperty());

    HBox homePageFrame = new HBox();
    homePageFrame.getChildren().addAll(buttonBoxFrame, contentFrame);

    HBox.setHgrow(buttonBoxFrame, Priority.ALWAYS);
    HBox.setHgrow(contentFrame, Priority.ALWAYS);

    buttonBoxFrame.prefWidthProperty().bind(homePageFrame.widthProperty().multiply(0.3));
    buttonBoxFrame.prefHeightProperty().bind(homePageFrame.heightProperty());

    contentFrame.prefWidthProperty().bind(homePageFrame.widthProperty().multiply(0.7));
    contentFrame.prefHeightProperty().bind(homePageFrame.heightProperty());


    transformationFrame.prefWidthProperty().bind(contentFrame.widthProperty());
    transformationFrame.prefHeightProperty().bind(contentFrame.heightProperty());

    HBox.setHgrow(contentFrame, Priority.ALWAYS);
    editFileFrame.prefHeightProperty().bind(contentFrame.heightProperty());

    settingsFrame.prefWidthProperty().bind(contentFrame.widthProperty());
    settingsFrame.prefHeightProperty().bind(contentFrame.heightProperty());

    this.getChildren().add(homePageFrame);
    getStylesheets().add("/stylesheets/transformationAttributes.css");
    getStylesheets().add("/stylesheets/buttonBox.css");
    update(NavigationState.MENU);
  }

  /**
   * Gets the buttonBoxMenu.
   *
   * @return The buttonBoxMenu.
   */
  public ButtonBoxMenu getButtonBoxMenu() {
    return buttonBoxMenu;
  }

  /**
   * Gets the buttonBoxControls.
   *
   * @return The buttonBoxControls.
   */
  public ButtonBoxControls getButtonBoxControls() {
    return buttonBoxControls;
  }

  /**
   * Gets the buttonBoxFiles.
   *
   * @return The buttonBoxFiles.
   */
  public ButtonBoxFiles getButtonBoxFiles() {
    return buttonBoxFiles;
  }

  /**
   * Gets the buttonBoxSettings.
   *
   * @return The buttonBoxSettings.
   */
  public ButtonBoxSettings getButtonBoxSettings() {
    return buttonBoxSettings;
  }

  /**
   * Gets the buttonBoxSelectGame.
   *
   * @return The buttonBoxSelectGame.
   */
  public ButtonBoxGames getButtonBoxGames() {
    return buttonBoxSelectGame;
  }

  /**
   * Gets the attributeControls.
   *
   * @return The attributeControls.
   */
  public AttributeControls getAttributeControls() {
    return attributeControls;
  }

  /**
   * Gets the editFileFrame.
   *
   * @return The editFileFrame.
   */
  public EditFileFrame getEditFileFrame() {
    return editFileFrame;
  }

  /**
   * Gets the settingsFrame.
   *
   * @return The settingsFrame.
   */
  public SettingsFrame getSettingsFrame() {
    return settingsFrame;
  }

  /**
   * Gets the affineControlsView.
   *
   * @return The affineControlsView.
   */
  public AffineControlsView getAffineControlsView() {
    return affineControlsView;
  }

  /**
   * Sets the transformation picture in the transformationFrame.
   *
   * @param chaosGame The chaosGame to set the transformation picture from.
   */
  public void setTransformationPicture(ChaosGame chaosGame) {
    contentFrame.getChildren().clear();
    transformationFrame.showTransformationPicture(chaosGame);
    contentFrame.getChildren().add(transformationFrame);
  }

  /**
   * Gets the juliaTransformationControls.
   *
   * @return The juliaTransformationControls.
   */
  public JuliaTransformationControls getJuliaTransformationControls() {
    return juliaTransformationControls;
  }

  /**
   * Sets the juliaTransformationControls.
   *
   * @param juliaTransformationControls The juliaTransformationControls to set.
   */
  public void setJuliaTransformationControls(
      JuliaTransformationControls juliaTransformationControls) {
    this.juliaTransformationControls = juliaTransformationControls;
  }

  /**
   * Updates the view based on the NavigationState.
   *
   * @param state The NavigationState to update the view based on.
   */
  @Override
  public void update(NavigationState state) {
    switch (state) {
      case MENU:
        //Clears the contentFrame and buttonBoxFrame and adds the menu buttons to the buttonBoxFrame
        buttonBoxFrame.getChildren().clear();
        contentFrame.getChildren().clear();
        buttonBoxFrame.setBottom(buttonBoxMenu);
        contentFrame.getChildren().add(logo);
        break;
      case SELECT_GAME:
        buttonBoxFrame.getChildren().clear();
        buttonBoxFrame.setBottom(buttonBoxSelectGame);
        break;
      case SELECT_GAME_FILES:
        buttonBoxFrame.setCenter(buttonBoxFiles.getFilesButtonsBox());
        break;
      case PLAY_AFFINE:
        contentFrame.getChildren().clear();
        //Sets the common attributes for the affine transformations
        buttonBoxFrame.setTop(attributeControls);
        //Sets the affine transformation controls as a scrollPane
        buttonBoxFrame.setCenter(affineControlsView.getScrollPaneAffineControls());
        //Sets the buttons to run and load new transformations
        buttonBoxFrame.setBottom(buttonBoxControls);
        break;
      case PLAY_JULIA:
        contentFrame.getChildren().clear();
        buttonBoxFrame.setTop(attributeControls);
        buttonBoxFrame.setCenter(juliaTransformationControls);
        buttonBoxFrame.setBottom(buttonBoxControls);
        break;
      case SELECT_FILE:
        buttonBoxFrame.setBottom(buttonBoxFiles);
        break;
      case SETTINGS:
        buttonBoxFrame.setBottom(buttonBoxSettings);
        contentFrame.getChildren().clear();
        contentFrame.getChildren().add(settingsFrame);
        break;
      case EDIT_FILE:
        contentFrame.getChildren().clear();
        contentFrame.getChildren().add(editFileFrame);
        break;

      default:
        break;
    }
  }
}