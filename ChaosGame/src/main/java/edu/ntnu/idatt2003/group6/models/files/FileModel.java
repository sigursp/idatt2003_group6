package edu.ntnu.idatt2003.group6.models.files;

import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.utils.ChaosGameFileHandler;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The FileModel class is a singleton
 * that manages the files containing the chaos game descriptions.
 * It is responsible for reading, writing, and deleting files.
 * It also notifies observers when the amount of files changes.
 * The class is part of the Model-View-Controller pattern.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class FileModel {
  private static final Logger LOGGER = Logger.getLogger(FileModel.class.getName());
  private static FileModel instance = null;
  private final List<FileObserver> observers;
  private FileState state;

  /**
   * Constructor for the FileModel class.
   */
  private FileModel() {
    observers = new ArrayList<>();
  }

  /**
   * Returns the instance of the FileModel class.
   * Following the singleton design principle.
   *
   * @return the instance of the FileModel class.
   */
  public static FileModel getInstance() {
    if (instance == null) {
      instance = new FileModel();
    }
    return instance;
  }

  /**
   * Returns a list of the files in the descriptions folder.
   *
   * @return List of file names in the descriptions folder.
   * @throws IllegalArgumentException if the files could not be found.
   */
  public List<String> getFiles() throws IllegalArgumentException {
    try {
      return ChaosGameFileHandler.showFiles(FilePath.DESCRIPTIONS.getPath());
    } catch (Exception e) {
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Could not find files @" + e.getMessage());
    }
  }

  /**
   * Adds a file to the descriptions' folder.
   *
   * @param fileName  the name of the file to be added.
   * @param chaosGame the chaos game description to be written to the file.
   * @throws IllegalArgumentException if the file could not be written.
   */
  public void addFile(String fileName, ChaosGameDescription chaosGame)
      throws IllegalArgumentException {
    if (fileName == null || chaosGame == null) {
      throw new IllegalArgumentException("File name and chaos game description cannot be null");
    }
    setState(FileState.FILES_UPDATING);
    try {
      ChaosGameFileHandler.writeChaosGameDescriptionToFile(
          FilePath.DESCRIPTIONS.getPath() + fileName, chaosGame);
      setState(FileState.FILES_CHANGED);
    } catch (Exception e) {
      setState(FileState.FILES_UNCHANGED);
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Could not write file @" + e.getMessage());
    }
  }

  /**
   * Returns a ChaosGameDescription object from a file.
   *
   * @param fileName the name of the file to be read.
   * @return the ChaosGameDescription object from the file.
   * @throws IllegalArgumentException if the file could not be read or if formatting is wrong.
   */
  public ChaosGameDescription getFile(String fileName) {
    if (fileName == null) {
      throw new IllegalArgumentException("File name cannot be null");
    }
    try {
      return ChaosGameFileHandler.readChaosGameDescriptionFromFile(
          FilePath.DESCRIPTIONS.getPath() + fileName);
    } catch (Exception e) {
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Could not read file @" + e.getMessage());
    }
  }

  /**
   * Removes a file from the descriptions' folder.
   *
   * @param fileName the name of the file to be removed.
   * @throws IllegalArgumentException if the file could not be deleted.
   */
  public void removeFile(String fileName) throws IllegalArgumentException {
    if (fileName == null) {
      throw new IllegalArgumentException("File name cannot be null");
    }
    setState(FileState.FILES_UPDATING);
    try {
      ChaosGameFileHandler.deleteFile(FilePath.DESCRIPTIONS.getPath() + fileName);
      setState(FileState.FILES_CHANGED);

    } catch (Exception e) {
      setState(FileState.FILES_UNCHANGED);
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Could not delete file @" + e.getMessage());
    }
  }

  /**
   * Sets the state of the FileModel.
   *
   * @param state the state to be set.
   * @throws IllegalArgumentException if the state is null.
   */
  public void setState(FileState state) throws IllegalArgumentException {
    if (state == null) {
      throw new IllegalArgumentException("State cannot be null");
    }
    if (!state.equals(this.state)) {
      this.state = state;
      notifyObservers();
    }
  }

  /**
   * Adds an observer to be notified when the state of the FileModel changes.
   *
   * @param observer the observer to be added.
   */
  public void addObserver(FileObserver observer) {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null");
    }
    observers.add(observer);
  }

  /**
   * Removes an observer from the list of observers.
   *
   * @param observer the observer to be removed.
   */
  public void removeObserver(FileObserver observer) {
    if (observer == null) {
      throw new IllegalArgumentException("Observer cannot be null");
    }
    observers.remove(observer);
  }

  /**
   * Notifies all observers that the state of the FileModel has changed.
   */
  private void notifyObservers() {
    observers.forEach(observer -> observer.update(state));
  }


}
