package edu.ntnu.idatt2003.group6.view.alert;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGameController;
import java.util.Objects;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * A class for creating an error alert.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class AlertError extends Alert {

  /**
   * Creates an error alert with the specified message.
   *
   * @param message the message to be displayed in the alert
   */
  public AlertError(String message, String description) {
    super(AlertType.ERROR);
    this.getDialogPane().getStylesheets().add(
        Objects.requireNonNull(getClass().getResource("/stylesheets/alert.css")).toExternalForm());
    this.setTitle("Error");
    setIcon();
    this.setGraphic(null);
    this.setHeaderText(null);

    Text text = new Text(message);
    text.getStyleClass().add("errorText");
    Text descriptionText = new Text(description);
    descriptionText.getStyleClass().add("errorDescription");

    VBox content = new VBox();
    content.getStyleClass().add("vBox");
    content.setPadding(new Insets(10));
    content.setSpacing(10);
    content.getChildren().addAll(text, descriptionText);

    this.getDialogPane().setContent(content);

    this.getDialogPane().lookupButton(ButtonType.OK).getStyleClass().add("positiveButton");
  }

  /**
   * Shows the alert.
   */
  public void showAlertError() {
    this.showAndWait();
  }

  /**
   * Sets the icon of the alert.
   */
  private void setIcon() {
    ((Stage) this.getDialogPane().getScene().getWindow()).getIcons().add(
        new Image(Objects.requireNonNull(
            ChaosGameController.class.getResourceAsStream("/Logo.png"))));
  }
}
