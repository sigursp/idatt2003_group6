package edu.ntnu.idatt2003.group6.models.navigation;

/**
 * Interface for an observer of the NavigationState.
 *
 * @version 3.2
 * @see NavigationState
 * @see NavigationModel
 * @since 3.2
 */
public interface NavigationObserver {
  void update(NavigationState state);
}
