package edu.ntnu.idatt2003.group6.models.chaosgame;

import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.transformation.AffineTransform2D;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;

/**
 * This class represents a canvas for rendering a fractal. It contains a 2D array of pixels, and
 * methods to get and set pixels, and to clear the canvas.
 *
 * @version 0.2.2
 * @since 0.2.2
 */
public class ChaosCanvas {
  private final int[][] canvas;
  private final int width;
  private final int height;
  private final Vector2D minCoords;
  private final Vector2D maxCoords;
  private final AffineTransform2D transformCoordsToIndices;

  /**
   * Constructor for the ChaosCanvas class. Creates a new canvas with the given width, height, and
   * coordinate bounds.
   *
   * @param width     The width of the canvas.
   * @param height    The height of the canvas.
   * @param minCoords The minimum coordinates of the canvas.
   * @param maxCoords The maximum coordinates of the canvas.
   * @throws IllegalArgumentException If the width or height is less than or equal to 0.
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords)
      throws IllegalArgumentException {
    if (width <= 0 || height <= 0) {
      throw new IllegalArgumentException("Width and height must be positive.");
    }
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.canvas = new int[height][width];
    this.transformCoordsToIndices = new
        AffineTransform2D(getMatrixForIndices(), getVectorForIndices());
  }

  /**
   * Returns the pixel value at the given point.
   *
   * @param point The point to get the pixel value at.
   * @return The pixel value at the given point.
   */
  public int getPixel(Vector2D point) {
    Vector2D transformedPoint = transformAndCheckPoint(point);
    return this.canvas[(int) transformedPoint.getX0()][(int) transformedPoint.getX1()];
  }

  /**
   * Sets the pixel value at the given point.
   *
   * @param point The point to set the pixel value at.
   */
  public void putPixel(Vector2D point) {
    Vector2D transformedPoint = transformAndCheckPoint(point);
    this.canvas[(int) transformedPoint.getX0()][(int) transformedPoint.getX1()]++;
  }

  /**
   * Sets the pixel value at the given point.
   *
   * @param point The point to get or set the pixel value at.
   * @return The pixel value at the given point.
   * @throws IllegalArgumentException If the point is out of bounds.
   */
  private Vector2D transformAndCheckPoint(Vector2D point) throws IllegalArgumentException {
    Vector2D transformedPoint = this.transformCoordsToIndices.transform(point);
    int x = (int) Math.round(transformedPoint.getX0());
    int y = (int) Math.round(transformedPoint.getX1());

    if (x < 0 || x >= width || y < 0 || y >= height) {
      throw new IllegalArgumentException("Point is out of bounds.");
    }
    return transformedPoint;
  }

  /**
   * Returns the canvas as a 2D array.
   *
   * @return The canvas as a 2D array.
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Clears the canvas by setting all pixels to 0.
   */
  public void clear() {
    for (int i = 0; i < this.width; i++) {
      for (int j = 0; j < this.height; j++) {
        this.canvas[j][i] = 0;
      }
    }
  }

  /**
   * Returns the affine transformation matrix.
   *
   * @return The affine transformation matrix.
   */
  private Matrix2x2 getMatrixForIndices() {
    return new Matrix2x2(
        0, (height - 1) / (minCoords.getX1() - maxCoords.getX1()),
        (width - 1) / (maxCoords.getX0() - minCoords.getX0()), 0);
  }

  /**
   * Returns the affine transformation vector.
   *
   * @return The affine transformation vector.
   */
  private Vector2D getVectorForIndices() {
    return new Vector2D(
        ((height - 1) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1()),
        ((width - 1) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0()));
  }
}
