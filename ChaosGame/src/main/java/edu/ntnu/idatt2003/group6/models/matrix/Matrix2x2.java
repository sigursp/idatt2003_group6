package edu.ntnu.idatt2003.group6.models.matrix;

import edu.ntnu.idatt2003.group6.models.vector.Vector2D;


/**
 * This record represents a 2x2 matrix.
 * It contains the components of the matrix and methods to multiply a vector by the matrix.
 *
 * @version 0.1.2
 * @since 0.1.2
 */
public record Matrix2x2(double a00, double a01, double a10, double a11) {
  /**
   * Constructor for the Matrix2x2 class. Creates a new matrix with the given components.
   *
   * @param a00 The a00 component of the matrix.
   * @param a01 The a01 component of the matrix.
   * @param a10 The a10 component of the matrix.
   * @param a11 The a11 component of the matrix.
   */
  public Matrix2x2 {
    // The record class automatically generates a constructor with the given components.
  }

  /**
   * Multiplies the given vector by this matrix.
   *
   * @param vector The vector to be multiplied by this matrix.
   * @return A new vector that is the result of the multiplication.
   * @throws IllegalArgumentException If the given vector is null or if its components are invalid.
   */
  public Vector2D multiply(Vector2D vector) throws IllegalArgumentException {
    if (vector == null) {
      throw new IllegalArgumentException("The given vector cannot be null.");
    }
    return new Vector2D(this.a00 * vector.getX0() + this.a01 * vector.getX1(),
        this.a10 * vector.getX0() + this.a11 * vector.getX1());
  }

}
