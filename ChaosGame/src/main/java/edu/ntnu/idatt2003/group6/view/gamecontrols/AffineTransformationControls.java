package edu.ntnu.idatt2003.group6.view.gamecontrols;

import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * A class that represents the controls for the affine transformations in the Chaos Game.
 * The class extends GridPane and contains TextFields for the affine transformation matrix A and
 * the vector B. The class also contains a Text object for the transformation number and a Label
 * for error messages.
 */
public class AffineTransformationControls extends GridPane {

  private final Text transformationNumber;
  private final TextField a00;
  private final TextField a01;
  private final TextField a10;
  private final TextField a11;
  private final TextField x0;
  private final TextField x1;

  /**
   * Constructor for the AffineTransformationControls class.
   * Initializes the TextFields and Labels
   * for the affine transformation matrix A and the vector B.
   * Also sets the style classes for the
   * different nodes.
   */
  public AffineTransformationControls() {
    getStylesheets().add("/stylesheets/transformationAttributes.css");
    getStyleClass().add("frame");

    transformationNumber = new Text();
    a00 = new TextField();
    a01 = new TextField();
    a10 = new TextField();
    a11 = new TextField();
    x0 = new TextField();
    x1 = new TextField();

    a00.setPromptText("a00");
    a01.setPromptText("a01");
    a10.setPromptText("a10");
    a11.setPromptText("a11");
    x0.setPromptText("x0");
    x1.setPromptText("x1");

    add(transformationNumber, 0, 0);
    Text matrixA = new Text("A:");
    add(matrixA, 1, 0);
    add(a00, 2, 0);
    add(a01, 3, 0);
    add(a10, 2, 1);
    add(a11, 3, 1);

    Text vectorB = new Text("B:");
    add(vectorB, 4, 0);
    add(x0, 5, 0);
    add(x1, 5, 1);
    Label errorLabel = new Label();
    add(errorLabel, 1, 2);

    setColumnSpan(errorLabel, 5);

    List<Node> children = getChildren();
    for (Node node : children) {
      if (node instanceof Text) {
        if (node == transformationNumber) {
          node.getStyleClass().add("attributeNumber");
        } else {
          node.getStyleClass().add("attributeNames");
        }
      } else if (node instanceof TextField) {
        node.getStyleClass().add("textField");
      } else if (node instanceof Label) {
        node.getStyleClass().add("errorLabel");
      }
    }
  }

  /**
   * Method that sets the transformation number for the transformation.
   */
  public void setTransformationNumber(String transformationNumber) {
    this.transformationNumber.setText(transformationNumber);
  }

  /**
   * method to get the string for A00 in the matrix.
   *
   * @return the string for A00.
   */
  public String getA00() {
    return a00.getText();
  }

  /**
   * method to get the string for A01 in the matrix.
   *
   * @return the string for A01
   */
  public String getA01() {
    return a01.getText();
  }

  /**
   * method to get the string for A10 in the matrix.
   *
   * @return the string for A10.
   */
  public String getA10() {
    return a10.getText();
  }

  /**
   * method to get the string for A11 in the matrix.
   *
   * @return the string for A11.
   */
  public String getA11() {
    return a11.getText();
  }

  /**
   * method to get the string for X0 in the vector.
   *
   * @return the string for X0.
   */
  public String getX0() {
    return x0.getText();
  }

  /**
   * method to get the string for X1 in the vector.
   *
   * @return the string for X1.
   */
  public String getX1() {
    return x1.getText();
  }

  /**
   * Method to set the A00 value in the matrix.
   *
   * @param a00 the value to set.
   */
  public void setA00(String a00) {
    this.a00.setText(a00);
  }

  /**
   * Method to set the A01 value in the matrix.
   *
   * @param a01 the value to set.
   */
  public void setA01(String a01) {
    this.a01.setText(a01);
  }

  /**
   * Method to set the A10 value in the matrix.
   *
   * @param a10 the value to set.
   */
  public void setA10(String a10) {
    this.a10.setText(a10);
  }

  /**
   * Method to set the A11 value in the matrix.
   *
   * @param a11 the value to set.
   */
  public void setA11(String a11) {
    this.a11.setText(a11);
  }

  /**
   * Method to set the X0 value in the vector.
   *
   * @param x0 the value to set.
   */
  public void setX0(String x0) {
    this.x0.setText(x0);
  }

  /**
   * Method to set the X1 value in the vector.
   *
   * @param x1 the value to set.
   */
  public void setX1(String x1) {
    this.x1.setText(x1);
  }
}
