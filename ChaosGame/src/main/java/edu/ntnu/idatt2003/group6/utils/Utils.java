package edu.ntnu.idatt2003.group6.utils;


import java.util.List;
import java.util.Scanner;

/**
 * Utility class to contain static methods used in the different classes of the project.
 * Mainly methods to verify parameters.
 *
 * @version 0.1.0
 * @since 0.1.0
 */
public class Utils {

  /**
   * Verifies if the given parameter is a valid sign, +-1.
   *
   * @param parameter     The parameter to be verified.
   * @param parameterName The name of the parameter to be used in the exception message.
   * @throws IllegalArgumentException If the given parameter is not a valid sign.
   */
  public static void verifyInt(int parameter, String parameterName)
      throws IllegalArgumentException {
    if (parameter != 1 && parameter != -1) {
      throw new IllegalArgumentException("The string for the parameter "
          + parameterName + " was not valid, try to register again\n"
          + "The sign must be either 1 or -1");
    }
  }

  /**
   * A method to input a double form the user. Verifies if the input is a valid double.
   *
   * @return The double input by the user.
   */
  public static double inputDouble() {
    Scanner in = new Scanner(System.in);
    while (!in.hasNextDouble()) {
      System.out.println("Invalid number, try again");
      in.next();
    }
    return in.nextDouble();
  }

  /**
   * A method to input an integer from the user. Verifies if the input is a valid integer.
   *
   * @return The integer input by the user.
   */
  public static int inputInt() {
    Scanner in = new Scanner(System.in);
    while (!in.hasNextInt()) {
      System.out.println("Invalid number, try again");
      in.next();
    }
    return in.nextInt();
  }

  /**
   * A method to print a list of strings with a number in front.
   */
  public static void printListWithNumbers(List<String> list) {
    for (int i = 0; i < list.size(); i++) {
      System.out.println(i + 1 + ". " + list.get(i));
    }
  }

  /**
   * A method to verify if the user input is y or n.
   *
   * @return true if the user input is y, false if the user input is n.
   */
  public static boolean editYesNo() {
    Scanner in = new Scanner(System.in);
    String choice = in.nextLine();
    while (!choice.equals("y") && !choice.equals("n")) {
      System.out.println("Invalid choice. Must be y/n.");
    }
    return choice.equals("y");
  }

  /**
   * a method to parse a string to an integer.
   *
   * @param input the string to be parsed.
   * @return the integer value of the string, or 0 if the string is not a valid integer.
   * @throws IllegalArgumentException if the string is not a valid integer.
   */
  public static int inputStringToInt(String input) throws IllegalArgumentException {
    try {
      return Integer.parseInt(input);
    } catch (NumberFormatException e) {
      throw new IllegalArgumentException("The string was not a valid integer");
    }
  }

  /**
   * a method to parse a string to a double.
   *
   * @param input the string to be parsed.
   * @return the double value of the string, or 0 if the string is not a valid double.
   * @throws IllegalArgumentException if the string is not a valid double.
   */
  public static double inputStringToDouble(String input) throws IllegalArgumentException {
    try {
      return Double.parseDouble(input);
    } catch (NullPointerException e) {
      throw new IllegalArgumentException("The string was not a valid double");
    }
  }
}
