package edu.ntnu.idatt2003.group6.controller.chaosgame;


import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.getAttributesAffineControlsList;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.getAttributesJuliaControls;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.getMaxCoordsControls;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.getMinCoordsControls;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.setAttributesAffineControlsList;
import static edu.ntnu.idatt2003.group6.utils.Utils.inputStringToInt;

import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.files.FileModel;
import edu.ntnu.idatt2003.group6.models.transformation.JuliaTransform;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import edu.ntnu.idatt2003.group6.utils.ControllerUtils;
import edu.ntnu.idatt2003.group6.view.alert.AlertError;
import edu.ntnu.idatt2003.group6.view.alert.AlertInputFileName;
import edu.ntnu.idatt2003.group6.view.frames.HomePage;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AffineControlsView;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AffineTransformationControls;
import edu.ntnu.idatt2003.group6.view.gamecontrols.JuliaTransformationControls;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;


/**
 * The controller for the ChaosGame model. This class is responsible for running the chaos game and
 * updating the view the result.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class ChaosGameController implements ChaosGameObserver {
  private static final Logger LOGGER = Logger.getLogger(ChaosGameController.class.getName());
  private ChaosGame model;
  private final HomePage view;
  private GameType gameType;
  private final AffineControlsView affineControlsView;
  private final FileModel fileModel = FileModel.getInstance();

  /**
   * Creates a new ChaosGameController.
   *
   * @param model The model for the chaos game.
   * @param view  The view for the chaos game.
   */
  public ChaosGameController(ChaosGame model, HomePage view) {
    this.model = model;
    this.view = view;
    this.gameType = model.getGameType();
    this.affineControlsView = view.getAffineControlsView();
    model.addObserver(this);

    setAttributesView();
    setButtonActions();
  }

  /**
   * Sets the actions for the buttons in the view.
   */
  private void setButtonActions() {
    view.getButtonBoxControls().getRunGameButton().setOnAction(e -> {
      try {
        this.runGame();
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });
    view.getButtonBoxControls().getLoadParametersButton().setOnAction(e -> {
      try {
        this.loadNewParameter();
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });
    view.getButtonBoxControls().getSaveButton().setOnAction(e -> {
      try {
        this.saveFile();
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });
    affineControlsView.getAddTransformButton().setOnAction(e -> {
      try {
        this.addAffineTransformation();
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });
    affineControlsView.getRemoveTransformButton().setOnAction(e -> {
      try {
        this.removeAffineTransformation();
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });
  }

  /**
   * Sets the attributes for the view.
   */
  private void setAttributesView() {
    try {
      setAttributesCommon();
      if (gameType == GameType.JULIA) {
        setJuliaAttributes();
      } else {
        setAffineAttributes();
      }
    } catch (Exception e) {
      String message = e.getMessage().split("@")[0];
      String description = e.getMessage().split("@")[1];
      AlertError alertError = new AlertError(message, description);
      alertError.showAlertError();
    }
  }

  /**
   * Sets the common attributes in a chaos game for the view.
   */
  private void setAttributesCommon() {
    try {
      ControllerUtils.setAttributesCommon(view.getAttributeControls(),
          model.getChaosGameDescription());
    } catch (Exception e) {
      LOGGER.warning("Error setting common attributes");
      throw new IllegalArgumentException("Error setting common attributes @" + e.getMessage());
    }
  }

  /**
   * Sets the attributes for the Julia transformation in the view.
   */
  private void setJuliaAttributes() {
    JuliaTransformationControls juliaView = view.getJuliaTransformationControls();

    JuliaTransform julia =
        (JuliaTransform) model.getChaosGameDescription().getTransformations().getFirst();
    juliaView.setImaginaryC(String.valueOf(julia.getPoint().getX1()));
    juliaView.setRealC(String.valueOf(julia.getPoint().getX0()));

    view.setJuliaTransformationControls(juliaView);
  }

  /**
   * Sets the attributes for the affine transformations in the view.
   *
   * @throws IllegalArgumentException If an error occurs while setting the affine attributes.
   */
  private void setAffineAttributes() throws IllegalArgumentException {
    try {
      List<AffineTransformationControls> affineControls = setAttributesAffineControlsList(
          model.getChaosGameDescription());
      affineControlsView.setAffineControlsList(affineControls);
    } catch (Exception e) {
      LOGGER.warning("Error setting affine attributes");
      throw new IllegalArgumentException("Error setting affine attributes @" + e.getMessage());
    }
  }

  /**
   * Adds an affine transformation to the view.
   */
  private void addAffineTransformation() {
    try {
      List<AffineTransformationControls> affineTransformList =
          affineControlsView.getAffineControlsList();
      affineTransformList.add(new AffineTransformationControls());
      affineControlsView.setAffineControlsList(affineTransformList);
    } catch (Exception e) {
      LOGGER.warning("Error adding affine transformation");
      throw new IllegalArgumentException("Error adding affine transformation @" + e.getMessage());
    }
  }

  /**
   * Removes an affine transformation from the view.
   */
  private void removeAffineTransformation() {
    try {
      List<AffineTransformationControls> affineTransformList =
          affineControlsView.getAffineControlsList();
      if (affineTransformList.size() > 1) {
        affineTransformList.removeLast();
      }
      affineControlsView.setAffineControlsList(affineTransformList);
    } catch (Exception e) {
      LOGGER.warning("Error removing affine transformation");
      throw new IllegalArgumentException("Error removing affine transformation @" + e.getMessage());
    }
  }

  /**
   * runs the chaos game.
   */
  private void runGame() {
    try {
      model.getCanvas().clear();
      int steps = inputStringToInt(view.getAttributeControls().getStepsField());
      model.runSteps(steps);
    } catch (Exception e) {
      LOGGER.warning("Error running game");
      throw new IllegalArgumentException("Error running game @" + e.getMessage());
    }
  }

  /**
   * Saves the game parameters to a file.
   */
  private void loadNewParameter() throws IllegalArgumentException {
    try {
      Vector2D minCoords = getMinCoordsControls(view.getAttributeControls());
      Vector2D maxCoords = getMaxCoordsControls(view.getAttributeControls());
      if (gameType == GameType.JULIA) {
        loadJulia(minCoords, maxCoords);
      } else {
        loadAffine(minCoords, maxCoords);
      }
    } catch (Exception e) {
      LOGGER.warning("Error loading new parameters");
      throw new IllegalArgumentException("Error loading new parameters @" + e.getMessage());
    }
  }

  /**
   * Loads a Julia transformation.
   *
   * @param minCoords the minimum coordinates for the transformation.
   * @param maxCoords the maximum coordinates for the transformation.
   * @throws IllegalArgumentException If an error occurs while loading the Julia transformation.
   */
  private void loadJulia(Vector2D minCoords, Vector2D maxCoords) throws IllegalArgumentException {
    model = new ChaosGame(getAttributesJuliaControls(
        view.getJuliaTransformationControls(), minCoords, maxCoords),
        1000, 1000);
    model.addObserver(this);
    this.gameType = model.getGameType();
    setAttributesView();
  }

  /**
   * Loads an affine transformation.
   *
   * @param minCoords the minimum coordinates for the transformation.
   * @param maxCoords the maximum coordinates for the transformation.
   * @throws IllegalArgumentException If an error occurs while loading the affine transformation.
   */
  private void loadAffine(Vector2D minCoords, Vector2D maxCoords) throws IllegalArgumentException {
    List<AffineTransformationControls> affineControls = affineControlsView.getAffineControlsList();
    model = new ChaosGame(getAttributesAffineControlsList(
        affineControls, minCoords, maxCoords),
        1000, 1000);
    model.addObserver(this);

    setAttributesView();
  }

  /**
   * Saves the game parameters to a file.
   *
   * @throws IllegalArgumentException If an error occurs while saving the file.
   */
  private void saveFile() throws IllegalArgumentException {
    AlertInputFileName alert = new AlertInputFileName();
    alert.showAlert();
    String filename = alert.getFileName();

    //Todo: Add try catch
    if (filename == null || filename.isEmpty()) {
      throw new IllegalArgumentException("Save to file did not execute @"
          + " Filename cannot be empty");
    } else {
      try {
        fileModel.addFile(filename, model.getChaosGameDescription());
      } catch (Exception e) {
        LOGGER.warning("Error saving file");
        throw new IllegalArgumentException("Error saving file @" + e.getMessage());
      }
    }
  }


  /**
   * Updates the view with the transformation picture.
   */
  private void updateView() {
    view.setTransformationPicture(model);
  }

  /**
   * Updates the controller based on the game state.
   *
   * @param state The state of the game.
   */
  @Override
  public void update(GameState state) {
    if (Objects.requireNonNull(state) == GameState.DONE) {
      updateView();
    }
  }
}
