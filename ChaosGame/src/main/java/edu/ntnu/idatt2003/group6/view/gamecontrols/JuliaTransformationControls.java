package edu.ntnu.idatt2003.group6.view.gamecontrols;

import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

/**
 * A class that represents the controls for the affine transformations in the Chaos Game.
 *
 * @version 0.3.2
 * @see JuliaTransformationControls
 */
public class JuliaTransformationControls extends GridPane {

  private final TextField realC;
  private final TextField imaginaryC;

  /**
   * Constructor for the JuliaTransformationControls class.
   */
  public JuliaTransformationControls() {
    getStylesheets().add("/stylesheets/transformationAttributes.css");
    getStyleClass().add("frame");

    realC = new TextField();
    imaginaryC = new TextField();

    realC.setPromptText("Real part of c");
    imaginaryC.setPromptText("Imaginary part of c");

    Text transformationNumber = new Text();
    add(transformationNumber, 0, 0);

    Text c = new Text("C:");
    add(c, 1, 0);
    add(realC, 2, 0);
    add(imaginaryC, 3, 0);

    List<Node> children = getChildren();
    for (Node node : children) {
      if (node instanceof Text) {
        if (node == transformationNumber) {
          node.getStyleClass().add("attributeNumber");
        } else {
          node.getStyleClass().add("attributeNames");
        }
      } else if (node instanceof TextField) {
        node.getStyleClass().add("textField");
      } else if (node instanceof Label) {
        node.getStyleClass().add("errorLabel");
      }
    }
  }

  /**
   * Returns the real part of the complex number c.
   *
   * @return the real part of the complex number c.
   */
  public String getRealC() {
    return realC.getText();
  }

  /**
   * Returns the imaginary part of the complex number c.
   *
   * @return the imaginary part of the complex number c.
   */
  public String getImaginaryC() {
    return imaginaryC.getText();
  }

  /**
   * Sets the real part of the complex number c.
   *
   * @param realC the real part of the complex number c.
   */
  public void setRealC(String realC) {
    this.realC.setText(realC);
  }

  /**
   * Sets the imaginary part of the complex number c.
   *
   * @param imaginaryC the imaginary part of the complex number c.
   */
  public void setImaginaryC(String imaginaryC) {
    this.imaginaryC.setText(imaginaryC);
  }
}
