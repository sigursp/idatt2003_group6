package edu.ntnu.idatt2003.group6.models.transformation;

import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;

/**
 * This class represents an affine transformation in 2D space. It contains a 2x2 matrix and a 2D
 * vector, and methods to transform a 2D vector by the affine transformation.
 *
 * @version 0.3.2
 * @see Transform2D
 * @see Matrix2x2
 * @since 0.1.3
 */
public record AffineTransform2D(Matrix2x2 matrix, Vector2D vector) implements Transform2D {
  private static final GameType gameType = GameType.AFFINE;

  /**
   * Constructor for the AffineTransform2D class. Creates a new affine transformation with the given
   * matrix and vector.
   *
   * @param matrix The 2x2 matrix of the affine transformation.
   * @param vector The 2D vector of the affine transformation.
   * @throws IllegalArgumentException If the given matrix or vector are null.
   */
  public AffineTransform2D {
    if (matrix == null || vector == null) {
      throw new IllegalArgumentException("The matrix or vector cannot be null.");
    }
  }

  /**
   * Transforms the given vector by this affine transformation.
   *
   * @param vector The vector to be transformed by this affine transformation.
   * @return A new vector that is the result of the transformation.
   * @throws IllegalArgumentException If the given vector is null or if its components are invalid.
   */
  public Vector2D transform(Vector2D vector) {
    return this.matrix.multiply(vector).add(this.vector);
  }

  /**
   * Returns the matrix of this affine transformation.
   *
   * @return The 2x2 matrix of this affine transformation.
   */
  @Override
  public Matrix2x2 matrix() {
    return this.matrix;
  }

  /**
   * Returns the vector of this affine transformation.
   *
   * @return The 2D vector of this affine transformation.
   */
  @Override
  public Vector2D vector() {
    return this.vector;
  }

  /**
   * Returns the game type of this affine transformation.
   *
   * @return The game type of this affine transformation.
   */
  public GameType gameType() {
    return gameType;
  }
}
