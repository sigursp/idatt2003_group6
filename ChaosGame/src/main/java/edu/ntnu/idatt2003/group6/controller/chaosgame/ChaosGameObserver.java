package edu.ntnu.idatt2003.group6.controller.chaosgame;

/**
 * An interface for observing the state of the chaos game.
 * The observer will be notified when the state of the game changes.
 *
 * @version 0.3.1
 * @since 0.3.1
 */
public interface ChaosGameObserver {
  void update(GameState state);
}
