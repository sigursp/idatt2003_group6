package edu.ntnu.idatt2003.group6.view.frames;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGame;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosCanvas;
import javafx.geometry.Insets;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 * This class is a view for displaying the transformation picture of a chaos game.
 * The transformation picture is a heatmap of the pixels that have been visited by the chaos game.
 * The more times a pixel has been visited, the more red it will be.
 * The transformation picture is displayed in a frame with a border.
 *
 * @version 0.3.2
 * @see ChaosGame
 * @see ChaosCanvas
 * @since 0.3.2
 */
public class TransformationFrame extends StackPane {

  private final BorderPane frame;

  /**
   * Creates an instance of the TransformationFrame class.
   */
  public TransformationFrame() {
    frame = new BorderPane();
    setPadding(new Insets(10));
    frame.setBorder(new Border(new BorderStroke(
        Color.BLACK, BorderStrokeStyle.SOLID, new CornerRadii(10), new BorderWidths(1))));
    getChildren().add(frame);
  }

  /**
   * Show the transformation picture of a chaos game in the frame.
   *
   * @param chaosGame The chaos game to show the transformation picture of.
   */
  public void showTransformationPicture(ChaosGame chaosGame) {
    getChildren().clear();
    getChildren().add(frame);

    ChaosCanvas canvas = chaosGame.getCanvas();
    int[][] canvasArray = canvas.getCanvasArray();
    int width = canvasArray[0].length;
    int height = canvasArray.length;

    // Find the maximum pixel count
    int maxPixelCount = 0;
    for (int[] row : canvasArray) {
      for (int pixelCount : row) {
        if (pixelCount > maxPixelCount) {
          maxPixelCount = pixelCount;
        }
      }
    }

    WritableImage image = new WritableImage(width, height);
    PixelWriter pixelWriter = image.getPixelWriter();

    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        int pixelCount = canvasArray[y][x];
        // Map the pixel count to a hue value for the heatmap
        // If pixelCount is 0, set color to black
        if (pixelCount == 0) {
          pixelWriter.setColor(x, y, Color.grayRgb(20));
        } else {
          double hue =
              240 + (120 * (pixelCount / (double) maxPixelCount)); // 240 is blue, 360 is red
          Color color = Color.hsb(hue, 1.0, 1.0); // saturation and brightness are set to maximum
          pixelWriter.setColor(x, y, color);
        }
      }
    }

    /* puts the finished image inside a frame */
    ImageView imageView = new ImageView(image);
    frame.setCenter(imageView);
    Insets padding = this.getPadding();
    imageView.setPreserveRatio(true);
    imageView.fitWidthProperty().bind(
        frame.widthProperty().subtract(padding.getLeft() + padding.getRight()));
    imageView.fitHeightProperty().bind(
        frame.heightProperty().subtract(padding.getTop() + padding.getBottom()));
    imageView.setPreserveRatio(true);
    getChildren().add(imageView);
  }

}
