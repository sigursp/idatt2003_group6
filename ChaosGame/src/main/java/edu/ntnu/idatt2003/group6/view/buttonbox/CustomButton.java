package edu.ntnu.idatt2003.group6.view.buttonbox;


import javafx.scene.control.Button;

/**
 * CustomButton is a class that extends the Button class in JavaFX.
 * It is used to create buttons with a specific style class.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class CustomButton extends Button {

  public CustomButton(String text, String styleClass) {
    super(text);
    getStyleClass().add(styleClass);
  }
}
