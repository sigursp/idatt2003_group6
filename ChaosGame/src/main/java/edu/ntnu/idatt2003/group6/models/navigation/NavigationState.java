package edu.ntnu.idatt2003.group6.models.navigation;

import edu.ntnu.idatt2003.group6.controller.navigation.NavigationController;

/**
 * Enum class for the different states of the view. Used to navigate between different views.
 *
 * @version 3.2
 * @see NavigationModel
 * @see NavigationObserver
 * @see NavigationController
 * @since 3.2
 */
public enum NavigationState {
  SELECT_FILE,
  EDIT_FILE,
  SELECT_GAME,
  SELECT_GAME_FILES,
  PLAY_GAME,
  PLAY_JULIA,
  PLAY_AFFINE,
  SETTINGS,
  MENU
}
