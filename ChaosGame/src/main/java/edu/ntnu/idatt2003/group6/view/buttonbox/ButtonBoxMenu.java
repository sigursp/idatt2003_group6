package edu.ntnu.idatt2003.group6.view.buttonbox;

import javafx.scene.layout.VBox;

/**
 * A class for creating a VBox with buttons for the menu.
 * The buttons are Play Game, Files, Settings and Quit.
 * The buttons are styled with the class menuButton.
 * The VBox is styled with the class buttonBox.
 * The class extends VBox.
 *
 * @version 0.3.2
 * @see VBox
 * @see CustomButton
 * @see CustomButton
 * @since 0.3.2
 */
public class ButtonBoxMenu extends VBox {

  private static final String MENU_BUTTON_STYLE = "menuButton";
  private final CustomButton playGameButton;
  private final CustomButton filesButton;
  private final CustomButton settingsButton;
  private final CustomButton quitButton;


  /**
   * Constructor for the ButtonBoxMenu class.
   */
  public ButtonBoxMenu() {
    getStyleClass().add("buttonBox");

    playGameButton = new CustomButton("Play Game", MENU_BUTTON_STYLE);
    filesButton = new CustomButton("Files", MENU_BUTTON_STYLE);
    settingsButton = new CustomButton("Settings", MENU_BUTTON_STYLE);
    quitButton = new CustomButton("Quit", MENU_BUTTON_STYLE);

    getChildren().addAll(playGameButton, filesButton, settingsButton, quitButton);
  }


  /**
   * Method for getting the play Game Button.
   *
   * @return the playGameButton
   */
  public CustomButton getPlayGameButton() {
    return playGameButton;
  }

  /**
   * Method for getting the file Button.
   *
   * @return the filesButton
   */
  public CustomButton getFilesButton() {
    return filesButton;
  }

  /**
   * Method for getting the settings Button.
   *
   * @return the settingsButton
   */
  public CustomButton getSettingsButton() {
    return settingsButton;
  }

  /**
   * Method for getting the quit Button.
   *
   * @return the quit Button
   */
  public CustomButton getQuitButton() {
    return quitButton;
  }
}
