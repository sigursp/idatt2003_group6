package edu.ntnu.idatt2003.group6.view.buttonbox;

import javafx.scene.layout.VBox;

/**
 * A class for creating a VBox with buttons for selecting and starting a game, and returning to
 * the menu.
 *
 * @version 0.3.2
 * @see CustomButton
 * @since 0.1
 */
public class ButtonBoxGames extends VBox {
  private static final String MENU_BUTTON_STYLE = "menuButton";

  private final CustomButton newAffineButton;
  private final CustomButton newJuliaButton;
  private final CustomButton juliaButton;
  private final CustomButton sierpinskiButton;
  private final CustomButton barnsleyButton;
  private final CustomButton fileButton;
  private final CustomButton backtoMenuButton;

  /**
   * Constructor for PlayGameButtonBox.
   * Creates a VBox with text fields for steps, max cords and min cords, and buttons for saving,
   * loading and running the game and returning to the menu.
   */
  public ButtonBoxGames() {
    getStyleClass().add("buttonBox");
    newAffineButton = new CustomButton("New Affine", MENU_BUTTON_STYLE);
    newJuliaButton = new CustomButton("New Julia", MENU_BUTTON_STYLE);
    juliaButton = new CustomButton("Preset Julia Game", MENU_BUTTON_STYLE);
    sierpinskiButton = new CustomButton("Preset Sierpinski Game", MENU_BUTTON_STYLE);
    barnsleyButton = new CustomButton("Preset Barnsley Game", MENU_BUTTON_STYLE);
    fileButton = new CustomButton("Game from file", MENU_BUTTON_STYLE);
    backtoMenuButton = new CustomButton("Back to menu", MENU_BUTTON_STYLE);

    getChildren().addAll(newAffineButton, newJuliaButton, juliaButton, sierpinskiButton,
        barnsleyButton,
        fileButton, backtoMenuButton);
  }

  public CustomButton getNewAffineButtonButton() {
    return newAffineButton;
  }

  public CustomButton getNewJuliaButtonButton() {
    return newJuliaButton;
  }

  public CustomButton getJuliaButton() {
    return juliaButton;
  }

  public CustomButton getSierpinskiButton() {
    return sierpinskiButton;
  }

  public CustomButton getBarnsleyButton() {
    return barnsleyButton;
  }

  public CustomButton getFileButton() {
    return fileButton;
  }

  public CustomButton getBackToMenuButton() {
    return backtoMenuButton;
  }
}

