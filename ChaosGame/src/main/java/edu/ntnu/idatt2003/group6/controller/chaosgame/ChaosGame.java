package edu.ntnu.idatt2003.group6.controller.chaosgame;

import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosCanvas;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import edu.ntnu.idatt2003.group6.utils.exceptions.IllegalChaosGameException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class represents a chaos game. It contains a canvas for rendering the game, a description of
 * the game, and methods for running the game.
 *
 * @version 0.2.3
 * @since 0.2.3
 */
public class ChaosGame {
  public final Random random;
  private final ChaosCanvas canvas;
  private final ChaosGameDescription description;
  private final List<ChaosGameObserver> chaosGameObservers;
  private Vector2D currentPoint;
  private GameState state;

  /**
   * Constructor for the ChaosGame class. Creates a new chaos game with the given description and
   *
   * @param description The description of the chaos game.
   * @param width       The width of the canvas.
   * @param height      The height of the canvas.
   * @throws IllegalChaosGameException If any of the parameters is invalid.
   */
  public ChaosGame(ChaosGameDescription description, int width, int height)
      throws IllegalChaosGameException {
    try {
      if (description == null) {
        throw new IllegalArgumentException("Description cannot be null");
      }
      if (width <= 0) {
        throw new IllegalArgumentException("Width must be greater than 0");
      }
      if (height <= 0) {
        throw new IllegalArgumentException("Height must be greater than 0");
      }
      this.canvas = new ChaosCanvas(
          width, height, description.getMinCoords(), description.getMaxCoords());
      this.description = description;
      this.currentPoint = new Vector2D(0, 0);
      this.random = new Random();
      this.chaosGameObservers = new ArrayList<>();
    } catch (IllegalArgumentException e) {
      throw new IllegalChaosGameException("Illegal chaos game description", e);
    }
  }

  /**
   * Returns the state of the chaos game.
   *
   * @return The state of the chaos game.
   */
  public GameState getState() {
    return state;
  }

  /**
   * Sets the state of the chaos game.
   *
   * @param state The state of the chaos game.
   */
  public void setState(GameState state) {
    this.state = state;
    notifyObservers();
  }

  /**
   * Adds an observer to the chaos game.
   *
   * @param chaosGameObserver The observer to add.
   * @throws IllegalArgumentException If the observer is null.
   */
  public void addObserver(ChaosGameObserver chaosGameObserver) throws IllegalArgumentException {
    validateObserver(chaosGameObserver);
    chaosGameObservers.add(chaosGameObserver);
  }

  /**
   * Removes an observer from the chaos game.
   *
   * @param chaosGameObserver The observer to remove.
   * @throws IllegalArgumentException If the observer is null.
   */
  public void removeObserver(ChaosGameObserver chaosGameObserver) throws IllegalArgumentException {
    validateObserver(chaosGameObserver);
    chaosGameObservers.remove(chaosGameObserver);
  }

  /**
   * Validates the observer.
   *
   * @param chaosGameObserver The observer to validate.
   */
  private void validateObserver(ChaosGameObserver chaosGameObserver) {
    if (chaosGameObserver == null) {
      throw new IllegalArgumentException("Observer cannot be null");
    }
  }

  /**
   * Notifies the observers of the chaos game changing state.
   */
  private void notifyObservers() {
    chaosGameObservers.forEach(chaosGameObserver -> chaosGameObserver.update(state));
  }

  /**
   * Returns the canvas of the chaos game.
   *
   * @return The canvas of the chaos game.
   */
  public ChaosCanvas getCanvas() {
    return canvas;
  }

  /**
   * Returns the description of the chaos game.
   *
   * @param steps The number of steps to run the chaos game for.
   * @throws IllegalArgumentException If the number of steps is less than 0.
   */
  public void runSteps(int steps) throws IllegalArgumentException {
    if (steps < 1) {
      throw new IllegalArgumentException("Number of steps must be equal or greater than 1");
    }
    for (int i = 0; i < steps; i++) {
      setState(GameState.RUNNING);
      notifyObservers();
      int randomIndex = random.nextInt(description.getTransformations().size());
      currentPoint = description.getTransformations().get(randomIndex).transform(currentPoint);
      canvas.putPixel(currentPoint);
    }
    setState(GameState.DONE);
    notifyObservers();
  }

  /**
   * Returns the description of the chaos game.
   *
   * @return The description of the chaos game.
   */
  public ChaosGameDescription getChaosGameDescription() {
    return description;
  }

  /**
   * Returns the game type of the chaos game.
   *
   * @return The game type of the chaos game.
   */
  public GameType getGameType() {
    return description.getGameType();
  }

}
