package edu.ntnu.idatt2003.group6.models.vector;

/**
 * This class represents a 2D vector. It contains the components of the vector and methods to add,
 * subtract, and multiply the vector by a scalar.
 *
 * @version 0.1.2
 * @see Complex
 * @since 0.1.2
 */
public class Vector2D {
  private final double x0;
  private final double x1;

  /**
   * Creates a new vector with the given components.
   *
   * @param x0 The x0 component of the vector.
   * @param x1 The x1 component of the vector.
   * @throws IllegalArgumentException If the given components are invalid.
   */
  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Returns the x0 component of this vector.
   *
   * @return The x0 component of this vector.
   */
  public double getX0() {
    return x0;
  }

  /**
   * Returns the x1 component of this vector.
   *
   * @return The x1 component of this vector.
   */
  public double getX1() {
    return x1;
  }

  /**
   * Adds the given vector to this vector.
   *
   * @param other The vector to be added to this vector.
   * @return A new vector that is the result of the addition.
   * @throws IllegalArgumentException If the given vector is null or if its components are invalid.
   */
  public Vector2D add(Vector2D other) {
    verifyVector2D(other);
    return new Vector2D(this.x0 + other.x0, this.x1 + other.x1);
  }

  /**
   * Subtracts the given vector from this vector.
   *
   * @param other The vector to be subtracted from this vector.
   * @return A new vector that is the result of the subtraction.
   * @throws IllegalArgumentException If the given vector is null or if its components are invalid.
   */
  public Vector2D subtract(Vector2D other) {
    verifyVector2D(other);
    return new Vector2D(this.x0 - other.x0, this.x1 - other.x1);
  }

  /**
   * Multiplies the given vector by the given scalar.
   *
   * @param scalar The scalar to multiply the vector by.
   * @return A new vector that is the result of the multiplication.
   */
  public Vector2D multiplyScalar(int scalar) {
    return new Vector2D(this.x0 * scalar, this.x1 * scalar);
  }

  /**
   * Verifies that the given vector is not null and that its components are valid.
   *
   * @param vector The vector to be verified.
   * @throws IllegalArgumentException If the vector is null.
   */
  private void verifyVector2D(Vector2D vector) {
    if (vector == null) {
      throw new IllegalArgumentException("The vector can not be null");
    }
  }

}
