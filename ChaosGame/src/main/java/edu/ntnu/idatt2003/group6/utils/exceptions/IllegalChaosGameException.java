package edu.ntnu.idatt2003.group6.utils.exceptions;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGame;

/**
 * Exception thrown when an illegal operation is performed on a ChaosGame object.
 *
 * @version 0.3.2
 * @see ChaosGame
 * @see edu.ntnu.idatt2003.group6.utils.ChaosGameFileHandler
 * @since 0.3.2
 */
public class IllegalChaosGameException extends IllegalArgumentException {

  /**
   * Constructs an IllegalChaosGameException with the specified detail message and cause.
   *
   * @param message the detail message
   * @param cause the cause
   */
  public IllegalChaosGameException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Constructs an IllegalChaosGameException with the specified detail message.
   *
   * @param message the detail message
   */
  public IllegalChaosGameException(String message) {
    super(message);
  }
}
