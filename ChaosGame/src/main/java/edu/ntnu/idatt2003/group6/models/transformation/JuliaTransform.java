package edu.ntnu.idatt2003.group6.models.transformation;

import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.vector.Complex;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import edu.ntnu.idatt2003.group6.utils.Utils;

/**
 * This class represents a Julia transformation in 2D space. It contains a complex number
 * and a signature to determine the direction of the transformation, positive or negative
 * and methods to transform a 2D vector by the Julia transformation.
 *
 * @version 0.3.2
 * @since 0.1.3
 */
public class JuliaTransform implements Transform2D {
  private final Complex point;
  private final int sign;
  private static final GameType gameType = GameType.JULIA;

  /**
   * Constructor for the JuliaTransform class. Creates a new Julia transformation with the given
   * complex number and signature.
   *
   * @param point The complex number of the Julia transformation.
   * @param sign  The signature of the Julia transformation.
   * @throws IllegalArgumentException If the given complex number is null
   *                                  or if its components are invalid.
   *                                  Also, if the sign is not +-1.
   */
  public JuliaTransform(Complex point, int sign) throws IllegalArgumentException {
    if (sign != 1 && sign != -1) {
      throw new IllegalArgumentException("The sign must be either 1 or -1.");
    }
    Utils.verifyInt(sign, "sign");
    this.point = point;
    this.sign = sign;
  }

  public Complex getPoint() {
    return point;
  }

  public GameType gameType() {
    return gameType;
  }

  /**
   * Transforms the given vector by this Julia transformation.
   *
   * @param vector The vector to be transformed by this Julia transformation.
   * @return A new vector that is the result of the transformation.
   */
  @Override
  public Vector2D transform(Vector2D vector) {
    Vector2D z = vector.subtract(point);                    // z = z - c
    Complex complexZ = new Complex(z.getX0(), z.getX1());   // Casting to complex
    Complex sqrt = complexZ.sqrt();                         // sqrt(z)
    return new Vector2D(sign * sqrt.getX0(),
        sign * sqrt.getX1()); // Adjusting imaginary part by sign
  }
}
