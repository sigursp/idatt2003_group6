package edu.ntnu.idatt2003.group6.view.buttonbox;

import javafx.scene.layout.VBox;

/**
 * The ButtonBoxSettings class creates a VBox with buttons for the settings menu.
 * The buttons are for general settings, background settings,
 * music settings and returning to the menu.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class ButtonBoxSettings extends VBox {

  private static final String MENU_BUTTON_STYLE = "menuButton";
  private final CustomButton backToMenuButton;

  /**
   * Constructor for PlayGameButtonBox.
   * Creates a VBox with text fields for steps, max cords and min cords, and buttons for saving,
   * loading and running the game and returning to the menu.
   */
  public ButtonBoxSettings() {
    getStyleClass().add("buttonBox");

    backToMenuButton = new CustomButton("Back to menu", MENU_BUTTON_STYLE);

    getChildren().addAll(backToMenuButton);
  }

  /**
   * Returns the back to menu button.
   *
   * @return the back to menu button.
   */
  public CustomButton getBackToMenuButton() {
    return backToMenuButton;
  }
}
