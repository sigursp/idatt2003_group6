package edu.ntnu.idatt2003.group6.models.files;

/**
 * Interface for the FileObserver class.
 * The FileObserver class is used to observe changes in the FileState class.
 * The FileObserver class is part of the Observer pattern.
 *
 * @version 0.3.2
 * @see FileState
 * @see FileObserver
 * @since 0.3.2
 */
public interface FileObserver {
  void update(FileState state);
}
