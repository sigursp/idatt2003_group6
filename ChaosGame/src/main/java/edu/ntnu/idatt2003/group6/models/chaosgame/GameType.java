package edu.ntnu.idatt2003.group6.models.chaosgame;

import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGame;
import edu.ntnu.idatt2003.group6.controller.chaosgame.ChaosGameController;

/**
 * Enum for the different types of games that can be played.
 * Used to determine certain actions that are different for each game type.
 * Mainly used in the ChaosGameDescriptionFactory class.
 * Or when the game type is needed in the ChaosGame class.
 * The main GameType is AFFINE and JULIA.
 *
 * @version 0.3.2
 * @since 0.3.2
 * @see ChaosGameDescriptionFactory
 * @see ChaosGame
 * @see ChaosGameController
 * @see edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription
 * @see edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescriptionFactory
 *
 */
public enum GameType {
  SIERPINSKI,
  BARNSLEY,
  JULIA,
  AFFINE,
  JULIA_NEW,
  AFFINE_NEW
}
