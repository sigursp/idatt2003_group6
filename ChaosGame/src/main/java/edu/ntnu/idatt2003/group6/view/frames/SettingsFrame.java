package edu.ntnu.idatt2003.group6.view.frames;

import edu.ntnu.idatt2003.group6.view.buttonbox.CustomToggleButton;
import java.util.Objects;
import javafx.geometry.Pos;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

/**
 * The SettingsFrame class creates a StackPane with a toggle group for music settings.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class SettingsFrame extends StackPane {
  private final ToggleGroup musicOption;
  private final CustomToggleButton musicOn;
  private final CustomToggleButton musicOff;
  private final Slider volumeSlider;

  /**
   * Constructor for SettingsFrame.
   * Creates a StackPane with a toggle group for music settings.
   */
  public SettingsFrame() {
    getStylesheets().add(Objects.requireNonNull(getClass()
        .getResource("/stylesheets/settings.css")).toExternalForm());



    musicOption = new ToggleGroup();
    musicOn = new CustomToggleButton("On", "optionButton");
    musicOff = new CustomToggleButton("Off", "optionButton");
    musicOn.setToggleGroup(musicOption);
    musicOff.setToggleGroup(musicOption);

    volumeSlider = new Slider(0, 0.2, 0.02);

    Text musicText = new Text("Music: ");
    musicText.getStyleClass().add("text");

    Text volumeText = new Text("Volume: ");
    volumeText.getStyleClass().add("text");

    HBox buttons = new HBox(musicOn, musicOff);
    GridPane musicPane = new GridPane();
    musicPane.add(musicText, 0, 0);
    musicPane.add(buttons, 1, 0);
    musicPane.add(volumeText, 0, 1);
    musicPane.add(volumeSlider, 1, 1);

    setAlignment(Pos.CENTER);
    getChildren().add(musicPane);

    musicPane.maxHeightProperty().bind(heightProperty().multiply(0.2));
    musicPane.maxWidthProperty().bind(widthProperty().multiply(0.5));
    musicPane.minWidthProperty().bind(widthProperty().multiply(0.5));
  }

  /**
   * Gets the toggle group for the option buttons.
   *
   * @return the toggle group
   */
  public ToggleGroup getMusicOption() {
    return musicOption;
  }

  /**
   * Gets the button for turning music on.
   *
   * @return the button for turning music on
   */
  public CustomToggleButton getMusicOn() {
    return musicOn;
  }

  /**
   * Gets the button for turning music off.
   *
   * @return the button for turning music off
   */
  public CustomToggleButton getMusicOff() {
    return musicOff;
  }

  /**
   * Gets the volume slider.
   *
   * @return the volume slider
   */
  public Slider getVolumeSlider() {
    return volumeSlider;
  }
}
