package edu.ntnu.idatt2003.group6.utils;

import static edu.ntnu.idatt2003.group6.utils.Utils.inputStringToDouble;

import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.matrix.Matrix2x2;
import edu.ntnu.idatt2003.group6.models.transformation.AffineTransform2D;
import edu.ntnu.idatt2003.group6.models.transformation.JuliaTransform;
import edu.ntnu.idatt2003.group6.models.transformation.Transform2D;
import edu.ntnu.idatt2003.group6.models.vector.Complex;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AffineTransformationControls;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AttributeControls;
import edu.ntnu.idatt2003.group6.view.gamecontrols.JuliaTransformationControls;
import java.util.ArrayList;
import java.util.List;

/**
 * This class contains methods to handle reading and writing ChaosGameDescriptions to and from
 * files.
 *
 * @version 0.3.2
 * @since 0.3.2
 * @see ChaosGameDescription
 * @see AffineTransformationControls
 * @see JuliaTransformationControls
 * @see AttributeControls
 * @see Vector2D
 * @see Transform2D
 * @see AffineTransform2D
 * @see JuliaTransform
 * @see Complex
 * @see Matrix2x2
 * @see Utils
 */
public class ControllerUtils {

  /**
   * Get the common attributes for a chaos game description
   * and put them in the EditFileFrame.
   *
   * @param attributeView The attribute view to get the attributes from.
   * @param model         The ChaosGameDescription model to get the attributes from.
   * @throws IllegalArgumentException If the model is null.
   */
  public static void setAttributesCommon(AttributeControls attributeView,
                                         ChaosGameDescription model)
      throws IllegalArgumentException {
    try {
      attributeView.setMinCoordsX0Field(String.valueOf(model.getMinCoords().getX0()));
      attributeView.setMinCoordsX1Field(String.valueOf(model.getMinCoords().getX1()));
      attributeView.setMaxCoordsX0Field(String.valueOf(model.getMaxCoords().getX0()));
      attributeView.setMaxCoordsX1Field(String.valueOf(model.getMaxCoords().getX1()));
    } catch (Exception e) {
      throw new IllegalArgumentException("Model is null: ");
    }
  }

  /**
   * Set the attributes for the Julia transformation in the JuliaFrame.
   *
   * @param chaosGameDescription The ChaosGameDescription to get the attributes from.
   * @return The JuliaTransformationControls frame with the attributes' set.
   * @throws IllegalArgumentException If the ChaosGameDescription is null.
   */
  public static JuliaTransformationControls setAttributesJulia(
      ChaosGameDescription chaosGameDescription) throws IllegalArgumentException {
    try {
      JuliaTransformationControls juliaView = new JuliaTransformationControls();
      JuliaTransform julia = (JuliaTransform) chaosGameDescription.getTransformations().getFirst();
      juliaView.setImaginaryC(String.valueOf(julia.getPoint().getX1()));
      juliaView.setRealC(String.valueOf(julia.getPoint().getX0()));
      return juliaView;
    } catch (Exception e) {
      throw new IllegalArgumentException("Attributes could not be set: @" + e.getMessage());
    }
  }

  /**
   * Set the attributes for the Affine transformations in the AffineFrame.
   *
   * @param model The ChaosGameDescription to get the attributes from.
   * @return The AffineTransformationControls frame with the attribute set.
   * @throws IllegalArgumentException If the ChaosGameDescription is null.
   */
  public static List<AffineTransformationControls> setAttributesAffineControlsList(
      ChaosGameDescription model) throws IllegalArgumentException {
    try {
      List<Transform2D> affineTransforms = model.getTransformations();
      List<AffineTransformationControls> affineViews = new ArrayList<>();
      affineTransforms.forEach(transform -> {
        AffineTransform2D affine = (AffineTransform2D) transform;
        AffineTransformationControls affineView = new AffineTransformationControls();
        affineView.setTransformationNumber(
            String.valueOf(affineTransforms.indexOf(transform) + 1));
        affineView.setA00(String.valueOf(affine.matrix().a00()));
        affineView.setA01(String.valueOf(affine.matrix().a01()));
        affineView.setA10(String.valueOf(affine.matrix().a10()));
        affineView.setA11(String.valueOf(affine.matrix().a11()));
        affineView.setX0(String.valueOf(affine.vector().getX0()));
        affineView.setX1(String.valueOf(affine.vector().getX1()));
        affineViews.add(affineView);
      });
      return affineViews;
    } catch (Exception e) {
      throw new IllegalArgumentException("View could not be set: @" + e.getMessage());
    }
  }

  /**
   * Get the attributes for a ChaosGameDescription from the EditFileFrame.
   *
   * @param affineTransformationControlsList The list of affine transformations to get the
   *                                         attributes from.
   * @param minCoords                        The minimum coordinates to get the attributes for.
   * @param maxCoords                        The maximum coordinates to get the attributes for.
   * @return The ChaosGameDescription with the found attributes.
   * @throws IllegalArgumentException If the ChaosGameDescription could not be created.
   */
  public static ChaosGameDescription getAttributesAffineControlsList(
      List<AffineTransformationControls> affineTransformationControlsList,
      Vector2D minCoords, Vector2D maxCoords) throws IllegalArgumentException {
    try {
      List<Transform2D> transforms = new ArrayList<>();
      affineTransformationControlsList.forEach(affine -> {
        double a00 = inputStringToDouble(affine.getA00());
        double a01 = inputStringToDouble(affine.getA01());
        double a10 = inputStringToDouble(affine.getA10());
        double a11 = inputStringToDouble(affine.getA11());
        double x0 = inputStringToDouble(affine.getX0());
        double x1 = inputStringToDouble(affine.getX1());
        Matrix2x2 matrix = new Matrix2x2(a00, a01, a10, a11);
        Vector2D vector = new Vector2D(x0, x1);
        AffineTransform2D affineTransform = new AffineTransform2D(matrix, vector);
        transforms.add(affineTransform);
      });
      return new ChaosGameDescription(transforms, minCoords, maxCoords);
    } catch (Exception e) {
      throw new IllegalArgumentException(
          "ChaosGameDescription could not be created: @" + e.getMessage());
    }
  }

  /**
   * Get the attributes for a ChaosGameDescription from the JuliaFrame.
   *
   * @param juliaTransform The JuliaTransformationControls to get the attributes from.
   * @param minCoords      The minimum coordinates to get the attributes for.
   * @param maxCoords      The maximum coordinates to get the attributes for.
   * @return The ChaosGameDescription with the found attributes.
   * @throws IllegalArgumentException If the ChaosGameDescription could not be created.
   */
  public static ChaosGameDescription getAttributesJuliaControls(
      JuliaTransformationControls juliaTransform, Vector2D minCoords, Vector2D maxCoords)
      throws IllegalArgumentException {
    try {
      List<Transform2D> transforms = new ArrayList<>();
      double realC = inputStringToDouble(juliaTransform.getRealC());
      double imaginaryC = inputStringToDouble(juliaTransform.getImaginaryC());
      Complex point = new Complex(realC, imaginaryC);
      JuliaTransform juliaPos = new JuliaTransform(point, 1);
      JuliaTransform juliaNeg = new JuliaTransform(point, -1);
      transforms.add(juliaPos);
      transforms.add(juliaNeg);
      return new ChaosGameDescription(transforms, minCoords, maxCoords);
    } catch (Exception e) {
      throw new IllegalArgumentException(
          "ChaosGameDescription could not be created: @" + e.getMessage());
    }
  }

  /**
   * Get the minimum coordinates from the AttributeControls.
   *
   * @param attributeControls The AttributeControls to get the minimum coordinates from.
   * @return The minimum coordinates.
   * @throws IllegalArgumentException If the minimum coordinates could not be found.
   */
  public static Vector2D getMinCoordsControls(AttributeControls attributeControls)
      throws IllegalArgumentException {
    try {
      double minCoordsX0 = inputStringToDouble(attributeControls.getMinCoordsX0Field());
      double minCoordsX1 = inputStringToDouble(attributeControls.getMinCoordsX1Field());
      return new Vector2D(minCoordsX0, minCoordsX1);
    } catch (Exception e) {
      throw new IllegalArgumentException("Could not get min coords: @" + e.getMessage());
    }
  }

  /**
   * Get the maximum coordinates from the AttributeControls.
   *
   * @param attributeControls The AttributeControls to get the maximum coordinates from.
   * @return The maximum coordinates.
   * @throws IllegalArgumentException If the maximum coordinates could not be found.
   */
  public static Vector2D getMaxCoordsControls(AttributeControls attributeControls)
      throws IllegalArgumentException {
    try {
      double maxCoordsX0 = inputStringToDouble(attributeControls.getMaxCoordsX0Field());
      double maxCoordsX1 = inputStringToDouble(attributeControls.getMaxCoordsX1Field());
      return new Vector2D(maxCoordsX0, maxCoordsX1);
    } catch (Exception e) {
      throw new IllegalArgumentException("Could not get max coords: @" + e.getMessage());
    }
  }
}