package edu.ntnu.idatt2003.group6.view.buttonbox;

import javafx.scene.control.ToggleButton;

/**
 * CustomToggleButton is a class that extends the ToggleButton class in JavaFX.
 * It is used to create toggle buttons with a specific style class.
 *
 * @version 0.3.2
 * @since 0.3.2
 */
public class CustomToggleButton extends ToggleButton {

  public CustomToggleButton(String text, String styleClass) {
    super(text);
    getStyleClass().add(styleClass);
  }
}
