package edu.ntnu.idatt2003.group6.view.gamecontrols;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

/**
 * The AffineControlsView class is a view class that creates a scrollable pane containing affine
 * transformation controls.
 * The class contains a list of affine transformation controls, and a function to add a new
 * transformation, a remove transformation button,
 * a scroll pane, and a VBox to contain the
 * affine transformation controls.
 *
 * @version 0.3.2
 * @see AffineTransformationControls
 * @since 0.3.2
 */
public class AffineControlsView {
  private final Button addTransformButton;
  private final Button removeTransformButton;
  private final ScrollPane scrollPaneAffineControls;
  private final VBox affineControlsBox;
  private ArrayList<AffineTransformationControls> affineControlsList;


  /**
   * Constructor for the AffineControlsView class.
   */
  public AffineControlsView() {
    this.affineControlsList = new ArrayList<>();
    this.addTransformButton = new Button("Add Transformation");
    this.removeTransformButton = new Button("Remove Transformation");
    this.affineControlsBox = new VBox();
    affineControlsBox.alignmentProperty().setValue(javafx.geometry.Pos.CENTER);
    affineControlsBox.getChildren().addAll(addTransformButton, removeTransformButton);
    this.scrollPaneAffineControls = makeScrollPaneAffineControls();
  }

  /**
   * Method to get a list of affine transformation controls.
   *
   * @return a list of affine transformation controls.
   */
  public List<AffineTransformationControls> getAffineControlsList() {
    return affineControlsList;
  }

  /**
   * Method to set a list of affine transformation controls in the view.
   *
   * @param affineControlsList a list of affine transformation controls.
   */
  public void setAffineControlsList(List<AffineTransformationControls> affineControlsList) {
    this.affineControlsList = (ArrayList<AffineTransformationControls>) affineControlsList;
    updateScrollPane();
  }

  /**
   * Method to add an affine transformation control to the view.
   *
   * @return the button to add an affine transformation control.
   */
  public Button getAddTransformButton() {
    return addTransformButton;
  }

  /**
   * Method to get the button to remove an affine transformation control.
   *
   * @return the button to remove an affine transformation control.
   */
  public Button getRemoveTransformButton() {
    return removeTransformButton;
  }

  /**
   * Method to update the scroll pane containing the affine transformation controls.
   */
  private void updateScrollPane() {
    scrollPaneAffineControls.setContent(makeScrollPaneAffineControls());
  }

  /**
   * Method to get the scroll pane containing the affine transformation controls.
   *
   * @return the scroll pane containing the affine transformation controls.
   */
  public ScrollPane getScrollPaneAffineControls() {
    return scrollPaneAffineControls;
  }

  /**
   * Method to make a scroll pane containing the affine transformation controls.
   *
   * @return a scroll pane containing the affine transformation controls.
   */
  private ScrollPane makeScrollPaneAffineControls() {
    ScrollPane scrollPane = new ScrollPane();
    scrollPane.getStyleClass().add("scrollPane");
    scrollPane.setFitToWidth(true);
    scrollPane.hbarPolicyProperty().setValue(ScrollPane.ScrollBarPolicy.NEVER);
    VBox collector = new VBox();
    collector.getStyleClass().add("vbox");
    for (AffineTransformationControls affineTransformationControl : affineControlsList) {
      collector.getChildren().add(affineTransformationControl);
    }

    int size = affineControlsList.size();
    if (size < 2) {
      removeTransformButton.setDisable(true);
      removeTransformButton.setVisible(false);
    } else {
      removeTransformButton.setDisable(false);
      removeTransformButton.setVisible(true);
    }
    collector.getChildren().add(affineControlsBox);
    scrollPane.setContent(collector);
    return scrollPane;
  }
}
