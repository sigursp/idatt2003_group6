package edu.ntnu.idatt2003.group6.models.files;

/**
 * Enum class for the file paths.
 * Contains the paths for the descriptions and images.
 * The paths are used to locate the files.
 *
 * @version 0.3.2
 * @see FileModel
 * @since 0.3.2
 */
public enum FilePath {
  DESCRIPTIONS("src/main/resources/descriptions/"),
  IMAGES("src/main/resources/images/"),

  MUSIC("src/main/resources/mp3/");

  private final String path;

  /**
   * Constructor for the FilePath class.
   *
   * @param path the path of the file.
   */
  FilePath(String path) {
    this.path = path;
  }

  /**
   * Returns the path of the file.
   *
   * @return the path of the file.
   */
  public String getPath() {
    return path;
  }
}
