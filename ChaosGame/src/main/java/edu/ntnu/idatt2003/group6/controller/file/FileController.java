package edu.ntnu.idatt2003.group6.controller.file;

import static edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescriptionFactory.createChaosGameDescription;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.getAttributesAffineControlsList;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.getAttributesJuliaControls;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.getMaxCoordsControls;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.getMinCoordsControls;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.setAttributesAffineControlsList;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.setAttributesCommon;
import static edu.ntnu.idatt2003.group6.utils.ControllerUtils.setAttributesJulia;

import edu.ntnu.idatt2003.group6.controller.navigation.NavigationController;
import edu.ntnu.idatt2003.group6.models.chaosgame.ChaosGameDescription;
import edu.ntnu.idatt2003.group6.models.chaosgame.GameType;
import edu.ntnu.idatt2003.group6.models.files.FileModel;
import edu.ntnu.idatt2003.group6.models.files.FileObserver;
import edu.ntnu.idatt2003.group6.models.files.FileState;
import edu.ntnu.idatt2003.group6.models.vector.Vector2D;
import edu.ntnu.idatt2003.group6.view.alert.AlertError;
import edu.ntnu.idatt2003.group6.view.buttonbox.ButtonBoxFiles;
import edu.ntnu.idatt2003.group6.view.buttonbox.CustomToggleButton;
import edu.ntnu.idatt2003.group6.view.frames.EditFileFrame;
import edu.ntnu.idatt2003.group6.view.frames.HomePage;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AffineControlsView;
import edu.ntnu.idatt2003.group6.view.gamecontrols.AffineTransformationControls;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * FileController is a class that controls the files in the application.
 * It is used to create, edit and delete files.
 * It is also used to save and load files.
 * It is a singleton class.
 * It implements the FileObserver interface.
 * It has a list of FileControllerObservers.
 *
 * @version 0.3.2
 * @see FileObserver
 * @see FileControllerObserver
 * @since 0.3.2
 */
public class FileController implements FileObserver {

  private static final Logger LOGGER = Logger.getLogger(FileController.class.getName());
  private static FileController instance = null;
  private final ButtonBoxFiles buttonBoxFiles;
  private final FileModel files;
  private final NavigationController controller;
  private final EditFileFrame editFileFrame;
  private final AffineControlsView affineControlsView;
  private final List<FileControllerObserver> observers = new ArrayList<>();
  private ChaosGameDescription chaosGameDescription;
  private Vector2D minCoords;
  private Vector2D maxCoords;
  private List<CustomToggleButton> fileButtons;


  /**
   * Constructor for FileController.
   *
   * @param view       HomePage of the application
   * @param controller NavigationController of the application
   */
  private FileController(HomePage view, NavigationController controller) {
    this.files = FileModel.getInstance();
    this.buttonBoxFiles = view.getButtonBoxFiles();
    this.controller = controller;
    editFileFrame = view.getEditFileFrame();
    affineControlsView = editFileFrame.getAffineControlsView();

    files.addObserver(this);

    buttonsController();
    showEditFileView();
    deleteFile();
    updateButtons();
  }

  /**
   * Returns an instance of the FileController class.
   * If the instance is null, it creates a new instance.
   * Following the singleton design pattern.
   *
   * @param view       HomePage of the application
   * @param controller NavigationController of the application
   * @return an instance of the FileController class
   */
  public static FileController getInstance(HomePage view, NavigationController controller) {
    if (instance == null) {
      instance = new FileController(view, controller);
    }
    return instance;
  }

  /**
   * Adds an observer to the list of observers.
   *
   * @param observer the observer to add
   */
  public void addObserver(FileControllerObserver observer) {
    observers.add(observer);
  }

  /**
   * Notifies all observers in the list of observers.
   */
  public void notifyObservers() {
    for (FileControllerObserver observer : observers) {
      observer.update();
    }
  }

  /**
   * Sets the actions of the buttons in the FileController.
   */
  private void buttonsController() {
    affineControlsView.getAddTransformButton().setOnAction(e -> {
      try {
        addTransformAffine();
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });

    affineControlsView.getRemoveTransformButton().setOnAction(e -> {
      try {
        removeTransformAffine();
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });

    editFileFrame.getSaveButton().setOnAction(e -> {
      try {
        updateFileAttributes();
      } catch (Exception ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });

    editFileFrame.getJuliaTransformButton().setOnAction(e -> {
      try {
        chaosGameDescription = createChaosGameDescription(GameType.JULIA_NEW);
        showFileAttributes();
      } catch (IllegalArgumentException ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });

    editFileFrame.getAffineTransformButton().setOnAction(e -> {
      try {
        chaosGameDescription = createChaosGameDescription(GameType.AFFINE_NEW);
        showFileAttributes();
      } catch (IllegalArgumentException ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });

    buttonBoxFiles.getNewFileButton().setOnAction(e -> {
      editFileFrame.clearFields();
      editFileFrame.setFileNameField("New file");
      buttonBoxFiles.getToggleButtons().selectToggle(null);
      controller.goToEditFile();
    });
  }

  /**
   * Adds a new affine transformation to the list of affine transformations.
   */
  private void addTransformAffine() {
    List<AffineTransformationControls> affineTransformationControlsList =
        affineControlsView.getAffineControlsList();
    AffineTransformationControls affineView = new AffineTransformationControls();
    affineView.setTransformationNumber(String.valueOf(affineTransformationControlsList.size() + 1));
    try {
      affineTransformationControlsList.add(affineView);
    } catch (Exception e) {
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Failed to add transformation @" + e.getMessage());
    }
    affineControlsView.setAffineControlsList(affineTransformationControlsList);
  }

  /**
   * Removes the last affine transformation from the list of affine transformations.
   */
  private void removeTransformAffine() {
    List<AffineTransformationControls> affineTransformationControlsList =
        affineControlsView.getAffineControlsList();
    try {
      affineTransformationControlsList.removeLast();
    } catch (Exception e) {
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Failed to remove transformation @" + e.getMessage());
    }
    affineControlsView.setAffineControlsList(affineTransformationControlsList);
  }

  /**
   * Shows the edit file view.
   */
  private void showEditFileView() {
    buttonBoxFiles.getEditFileButton().setOnAction(e -> {
      try {
        for (CustomToggleButton fileButton : fileButtons) {
          if (fileButton.isSelected()) {
            controller.goToEditFile();
            chaosGameDescription = files.getFile(fileButton.getText());
            editFileFrame.setFileNameField(fileButton.getText());
            showFileAttributes();
            break;
          }
        }
      } catch (IllegalArgumentException ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });
  }

  /**
   * Deletes a file from the list of files depending on the selected file.
   */
  private void deleteFile() {
    buttonBoxFiles.getDeleteFileButton().setOnAction(e -> {
      try {
        for (CustomToggleButton fileButton : fileButtons) {
          if (fileButton.isSelected()) {
            files.removeFile(fileButton.getText());

            break;
          }
        }
      } catch (IllegalArgumentException ex) {
        String message = ex.getMessage().split("@")[0];
        String description = ex.getMessage().split("@")[1];
        AlertError alertError = new AlertError(message, description);
        alertError.showAlertError();
        e.consume();
      }
    });
  }

  /**
   * Sets the attributes of the file.
   *
   * @throws IllegalArgumentException if the file does not contain the correct attributes.
   */
  private void showFileAttributes() throws IllegalArgumentException {
    //Sets min and max coords
    try {
      setAttributesCommon(editFileFrame.getAttributeControls(), chaosGameDescription);
    } catch (Exception e) {
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Failed to set file attributes @" + e.getMessage());
    }
    editFileFrame.showFileAttributes();
    //Checks game type
    try {
      if (chaosGameDescription.getGameType().equals(GameType.JULIA)) {
        showJuliaAttributes();
      } else if (chaosGameDescription.getGameType().equals(GameType.AFFINE)) {
        showAffineAttributes();
      }
    } catch (Exception e) {
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Failed to show file attributes @" + e.getMessage());
    }
  }

  /**
   * Sets the attributes of the Julia transformation.
   *
   * @throws IllegalArgumentException if the file does not contain the correct attributes.
   */
  private void showJuliaAttributes() throws IllegalArgumentException {
    editFileFrame.setJuliaTransformControls(
        setAttributesJulia(chaosGameDescription));
    editFileFrame.showJuliaAttributes();
  }

  /**
   * Sets the attributes of the affine transformations.
   *
   * @throws IllegalArgumentException if the file does not contain the correct attributes.
   */
  private void showAffineAttributes() throws IllegalArgumentException {
    List<AffineTransformationControls> affineControls = setAttributesAffineControlsList(
        chaosGameDescription);

    affineControlsView.setAffineControlsList(affineControls);
    editFileFrame.showAffineAttributes();
  }


  /**
   * Updates the file attributes.
   *
   * @throws IllegalArgumentException if the file does not contain the correct attributes.
   */
  private void updateFileAttributes() throws IllegalArgumentException {
    try {
      minCoords = getMinCoordsControls(editFileFrame.getAttributeControls());
      maxCoords = getMaxCoordsControls(editFileFrame.getAttributeControls());

      if (chaosGameDescription.getGameType().equals(GameType.JULIA)) {
        updateJuliaAttributes();
      } else {
        updateAffineAttributes();
      }

    } catch (Exception e) {
      LOGGER.severe(e.getMessage());
      throw new IllegalArgumentException("Failed to save file attributes, "
          + "File did not contain the correct attributes. @" + e.getMessage());
    }
  }

  /**
   * Updates the Julia attributes.
   *
   * @throws IllegalArgumentException if the view field does not contain the correct attributes.
   */
  private void updateJuliaAttributes() throws IllegalArgumentException {
    chaosGameDescription = getAttributesJuliaControls(
        editFileFrame.getJuliaTransformationControls(), minCoords, maxCoords);
    saveFile();
  }

  /**
   * Updates the affine attributes.
   *
   * @throws IllegalArgumentException if the view field does not contain the correct attributes.
   */
  private void updateAffineAttributes() throws IllegalArgumentException {
    chaosGameDescription = getAttributesAffineControlsList(
        editFileFrame.getAffineTransformControls(), minCoords, maxCoords);
    saveFile();
  }

  /**
   * Saves the file.
   *
   * @throws IllegalArgumentException if the file does not contain the correct attributes.
   */
  private void saveFile() throws IllegalArgumentException {
    String filename = editFileFrame.getFileNameField();
    files.addFile(filename, chaosGameDescription);
  }


  /**
   * Updates the buttons in the file controller.
   */
  private void updateButtons() {
    buttonBoxFiles.clearFileButtons();
    List<String> file = files.getFiles();
    file.forEach(buttonBoxFiles::addFileButton);
    fileButtons = buttonBoxFiles.getFileButtonsToggle();
  }


  /**
   * Updates the file buttons and notifies the observers.
   *
   * @param state the state of the file.
   */
  @Override
  public void update(FileState state) {
    if (Objects.requireNonNull(state) == FileState.FILES_CHANGED) {
      updateButtons();
      notifyObservers();
    }
  }
}
