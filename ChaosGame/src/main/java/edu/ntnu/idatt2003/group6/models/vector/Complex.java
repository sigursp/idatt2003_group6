package edu.ntnu.idatt2003.group6.models.vector;

/**
 * This class represents a complex number in 2D space. It contains the real and imaginary parts of
 * the complex number and methods to perform operations with complex numbers.
 *
 * @version 0.3.2
 * @see Vector2D
 * @since 0.1.2
 */
public class Complex extends Vector2D {

  /**
   * Constructor for the Complex class which extends the Vector2D class.
   * Creates a new complex number with the given real and
   * imaginary parts.
   *
   * @param realPart      The real part of the complex number.
   * @param imaginaryPart The imaginary part of the complex number.
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /**
   * Returns the square root of the given complex vector.
   *
   * @return A new complex vector that is the result of the square root.
   */
  public Complex sqrt() {
    double x = this.getX0();
    double y = this.getX1();
    // the core is square root of x^2 + y^2
    double core = Math.sqrt(x * x + y * y);
    // square root of 1/2 * (x^2 + y^2 + x)
    double realPart = Math.sqrt((core + x) / 2);
    // square root of i * 1/2 * (x^2 + y^2 - x)
    double imaginaryPart = Math.signum(y) * Math.sqrt((core - x) / 2);
    // return the new complex number
    return new Complex(realPart, imaginaryPart);
  }
}
